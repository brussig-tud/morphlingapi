
#ifndef __NETWORK_H__
#define __NETWORK_H__


//////
//
// Namespaces
//

// Implemented namespace
namespace Morphling {



//////
//
// Classes
//

/** @brief Common interface for IP-based communication. */
class IPTransport
{

protected:

	////
	// Data members

	/** @brief Implementation handle. */
	void* pimpl;


	////
	// Object construction / destruction

	/** @brief Used internally. */
	IPTransport(const String &peerIpAddress, unsigned port, void* sHandle);


public:

	////
	// Object construction / destruction

	/** @brief The destructor. */
	virtual ~IPTransport();


	////
	// Methods

	/**
	 * @brief Sends a UDP packet with the given payload.
	 *
	 * @param buffer
	 *		Pointer to the memory region containing the payload.
	 * @param length
	 *		Length, in bytes, of the payload located at @c buffer .
	 */
	virtual void send (const void *buffer, unsigned length) = 0;

	/**
	 * @brief Receives a UDP packet of given payload size.
	 *
	 * @return
	 *		@c true if data was received (which will always be the case if @c dontBlock
	 *		is set to @c false, and @c false otherwise.
	 *
	 * @param buffer
	 *		Pointer to the memory region to write the payload to.
	 * @param length
	 *		Length, in bytes, of the payload buffer.
	 * @param dontBlock
	 *		If set to @c true , @c recv will return immediatly if no data is waiting to be
	 *		received, leaving the @c buffer untouched.
	*/
	virtual bool recv (void *buffer, unsigned length, bool dontBlock=false) = 0;

	/** @brief Tr. */
	static String resolveHost (const String &host);
};

/** @brief Class encapsulating the UDP communication between two specific hosts. */
class UDP : public IPTransport
{

public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs with the given address and port.
	 *
	 * @param address
	 *		The IP address of the peer to communicate with in standard dottet notation
	 *		(e.g. "127.0.0.1"). Use @ref #resolveHost to obtain this IP address
	 *		string from a hostname (or any string referring to a valid host, even another
	 *		IP address).
	 * @param port
	 *		The TCP port on which the peer expects incoming datagrams.
	 */
	UDP(const String &peerIpAddress, unsigned port);

	/** @brief The destructor. */
	virtual ~UDP();


	////
	// Interface: IPTransport

	/**
	 * @brief Sends a UDP packet with the given payload.
	 *
	 * @param buffer
	 *		Pointer to the memory region containing the payload.
	 * @param length
	 *		Length, in bytes, of the payload located at @c buffer .
	 */
	virtual void send (const void *buffer, unsigned length);

	/**
	 * @brief Receives a UDP packet of given payload size.
	 *
	 * @return
	 *		@c true if data was received (which will always be the case if @c dontBlock
	 *		is set to @c false, and @c false otherwise.
	 *
	 * @param buffer
	 *		Pointer to the memory region to write the payload to.
	 * @param length
	 *		Length, in bytes, of the payload buffer.
	 * @param dontBlock
	 *		If set to @c true , @c recv will return immediatly if no data is waiting to be
	 *		received, leaving the @c buffer untouched.
	 */
	virtual bool recv (void *buffer, unsigned length, bool dontBlock=false);
};

/** @brief Class encapsulating the UDP communication between two specific hosts. */
class TCP : public IPTransport
{

public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs with the given address and port.
	 *
	 * @param address
	 *		The IP address of the peer to communicate with in standard dottet notation
	 *		(e.g. "127.0.0.1"). Use @ref #resolveHost to obtain this IP address
	 *		string from a hostname (or any string referring to a valid host, even another
	 *		IP address).
	 * @param port
	 *		The TCP port on which the peer expects incoming datagrams.
	 */
	TCP(const String &peerIpAddress, unsigned port);

	/** @brief The destructor. */
	virtual ~TCP();


	////
	// Interface: IPTransport

	/**
	 * @brief Sends a UDP packet with the given payload.
	 *
	 * @param buffer
	 *		Pointer to the memory region containing the payload.
	 * @param length
	 *		Length, in bytes, of the payload located at @c buffer .
	 */
	virtual void send (const void *buffer, unsigned length);

	/**
	 * @brief Receives a UDP packet of given payload size.
	 *
	 * @return
	 *		@c true if data was received (which will always be the case if @c dontBlock
	 *		is set to @c false, and @c false otherwise.
	 *
	 * @param buffer
	 *		Pointer to the memory region to write the payload to.
	 * @param length
	 *		Length, in bytes, of the payload buffer.
	 * @param dontBlock
	 *		If set to @c true , @c recv will return immediatly if no data is waiting to be
	 *		received, leaving the @c buffer untouched.
	 */
	virtual bool recv (void *buffer, unsigned length, bool dontBlock=false);
};


// Close implemented namespace
}


#endif // ifndef __NETWORK_H__
