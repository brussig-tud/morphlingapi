
//////
//
// Includes
//

// C++ STL and Boost
#if (_MSC_VER < 1910)
	#include "Boost/any.hpp"
#else
	#include <any>
#endif
#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>
#include <queue>
#include <string>
#include <sstream>
#include <memory>
#include <chrono>
#include <exception>
#include <stdexcept>

// Windows API
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>

// InvHandschuh SDK
#include "InvHandschuh/kommunikation_protokoll_invhand_v11.h"

// Local includes
#include "Utils.h"
#include "Thread.h"
#include "Network.h"

// Implemented header
#include "Communication.h"



//////
//
// Namespaces
//

// Implemented namespace
using namespace Morphling;



//////
//
// Module initialization and globals
//

// No "storage class specifier 'register' is deprecated" warnings
#pragma warning (disable : 5033)

// [BEGIN] Module-private global namespace
namespace {

// Utility for message parsing
template <class TC>
std::vector<std::basic_string<TC> > splitByToken (const TC* bytes, TC token)
{
	// Prepare output
	std::vector<std::basic_string<TC> > ret;
	ret.reserve(2);

	// Iterate through characters
	for (unsigned i=0, last=0; bytes[i]!=0; i++)
	{
		if (bytes[i] == token)
		{
			ret.emplace_back(&(bytes[last]), i-last);
			last = i+1;
		}
	}
	return std::move(ret);
}

// Linear interpolation
template <class T>
T lerp (T v1, T v2, double t) { return T(double(v1)*(1.0-t) + double(v2)*t); }

// [END] Module-private global namespace
}



//////
//
// Module-private classes
//

class MorphlingThread : public Thread
{

private:

	// UDP communication
	UDP udp;

	// State machine
	enum Runlevel
	{
		RL_POWEROFF, RL_INIT, RL_RUNNING, RL_SHUTDOWN
	} runlevel;
	Mutex rlvlMutex, initMutex, shutdownMutex;
	bool initialize, deinitialize;

	// Packet counter
	unsigned packetCounter;

	// Runlevel control
	Runlevel getRunlevel (void)
	{
		Runlevel level;
		{ Lock l(rlvlMutex);
		level = runlevel; }
		return level;
	}
	void setRunlevel (Runlevel level)
	{
		Lock l(rlvlMutex);
		runlevel = level;
	}


public:

	// Controls
	#define CURRENT_SET_PCOUNT 5
	Mutex peakMutex;
	unsigned setPeak;
	short peak;
	Mutex meanMutex;
	unsigned setMean;
	short mean;
	Mutex posTgtMutex;
	unsigned short actuators_tgt [5];
	Mutex posCurMutex;
	unsigned short actuators_cur [5];
	Mutex buttonThreshMutex;
	double buttonThreshold;
	MorphlingComms::MorphlingPressedCallback *onMorphlingPressed;
	Mutex timingMutex;
	unsigned period;
	double userFrametime;
	bool setPeriod;
	Mutex sineMutex;
	bool sineWave;
	double sinePos, sinePeriodLen, sineMoveSpeed;
	#define _SINE_PERIOD_LEN 200.0 // in millimeters
	#define _SINE_MOVE_SPEED 100.0 // in millimeters per second

	// Construct with address/port
	MorphlingThread(const TCHAR *serverAddress, unsigned port)
		: udp(IPTransport::resolveHost(serverAddress), port), runlevel(RL_RUNNING),
		  initialize(true), deinitialize(false), packetCounter(0),
		  rlvlMutex(L"MorphlingThread_rlvl__"+nameFromPointer(this)),
		  initMutex(L"MorphlingThread_init__" +nameFromPointer(this)),
		  shutdownMutex(L"MorphlingThread_shutdown__" +nameFromPointer(this)),
		  peakMutex(L"MorphlingThread_peak__"+nameFromPointer(this)),
		  meanMutex(L"MorphlingThread_mean__"+nameFromPointer(this)),
		  posTgtMutex(L"MorphlingThread_posTgt__" +nameFromPointer(this)),
		  posCurMutex(L"MorphlingThread_posCur__" +nameFromPointer(this)),
		  buttonThreshMutex(L"MorphlingThread_buttonThresh__" +nameFromPointer(this)),
		  timingMutex(L"MorphlingThread_timing__" +nameFromPointer(this)),
		  sineMutex(L"MorphlingThread_sine__" +nameFromPointer(this)),
		  buttonThreshold(0), onMorphlingPressed(nullptr), period(delay_UDP_schreiben),
		  userFrametime(0), setPeriod(false), sineWave(false), sinePos(65.0),
		  sinePeriodLen(_SINE_PERIOD_LEN), sineMoveSpeed(_SINE_MOVE_SPEED)
	{
		// Initialize actuator target and current positions
		for (unsigned i=0; i<5; i++)
			actuators_cur[i] = actuators_tgt[i] =
				MIN_VALUE_16Bit_POS + (MAX_VALUE_16Bit_POS-MIN_VALUE_16Bit_POS)/2;

		// Initialze peak/mean current setting indicators
		peak = 500;
		setPeak = 0/*CURRENT_SET_PCOUNT*/;
		mean = 350;
		setMean = 0/*CURRENT_SET_PCOUNT*/;
	}

	virtual ~MorphlingThread()
	{
		// We'll give the thread some time to exit gracefully
		wait(2048);
	}

	// (Re-)init control
	void startup (void)
	{
		Lock lR(rlvlMutex), lI(initMutex);
		//if (runlevel == RL_INIT)
		//	initialize = true;
		setRunlevel(RL_INIT);
	}
	void powerdown (void)
	{
		Lock lR(rlvlMutex), lS(shutdownMutex);
		if (runlevel == RL_INIT || runlevel == RL_RUNNING)
			deinitialize = true;
	}

	#define _PI 3.1415926535897932384626433832795028841971693993751058209749445923
	#define _PIx2 (_PI*2.0)
	unsigned short sine (double s)
	{
		double x = s * _PIx2/sinePeriodLen,
		       y = std::sin(x)*10.0 + 40.0;

		unsigned val =
			MIN_VALUE_16Bit_POS + unsigned(
				double(MAX_VALUE_16Bit_POS-MIN_VALUE_16Bit_POS)*y/80.0
			);
		return val;
	}

	unsigned run (void)
	{
		// Operate morphling in an exception-safe way by means of a RAII structure with
		// an overly dramatic name
		struct MorphlingKillswitch
		{
			unsigned &packetCounter;
			UDP &udp;
			bool operating;

			MorphlingKillswitch(UDP &udp, unsigned *packetCounter)
				: packetCounter(*packetCounter), udp(udp), operating(false)
			{
				// Startup the morphling
				tUDP_Paket_16bit packet/* = {0}*/;
				packet.SBP = SBP_FLAG_INIT;
				packet.PA = PAKET_INIT;
				packet.laufende_paket_nummer = (*packetCounter)++;
				packet.checksum = checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
				udp.send(&packet, UDP_PAKET_16bit_SIZE);
				operating = true;
			}
			~MorphlingKillswitch()
			{
				if (operating)
				{
					// Since we're here we have to assume the thread had an exception
					// - send emergency shutdown packet
					tUDP_Paket_16bit packet/* = {0}*/;
					packet.SBP = SBP_FLAG_EMMERGENCY;
					packet.PA = PAKET_SHUTDOWN;
					packet.laufende_paket_nummer = packetCounter++;
					packet.checksum = checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					try {
						udp.send(&packet, UDP_PAKET_16bit_SIZE);
					}
					catch (...)
					{
						// This is a last-ditch effort, so we want the destructor to
						// finish even in the case of an exception
					}
					operating = false;
				}
			}

			void disengage (void)
			{
				for (unsigned i=0; i<10; i++)
				{
					tUDP_Paket_16bit packet/* = {0}*/;
					packet.SBP = SBP_FLAG_HERUNTERFAHREN;
					packet.PA = PAKET_SHUTDOWN;
					packet.LDP_RD = PAKET_DATEN_AKTORPOS;
					packet.laufende_paket_nummer = packetCounter++;
					std::fill_n(packet.daten_16bit, 5, 150);
					packet.checksum = checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);
					Thread::sleep(1);
					packet.SBP = SBP_FLAG_INIT;
					packet.PA = PAKET_INIT;
					packet.LDP_RD = PAKET_DATEN_AKTORPOS;
					packet.laufende_paket_nummer = packetCounter++;
					std::fill_n(packet.daten_16bit, 5, 150);
					packet.checksum = checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);
					std::cout << "RESET!!!" << std::endl;
					Thread::sleep(1);
				}
				operating = false;
			}
		} killswitch(udp, &packetCounter);


		// Actuator change interpolation
		double curUserFrametime, tStep = 0;
		unsigned short curActuators_tgt [5], actuators_tgtStart [5];
		std::copy_n(actuators_cur, 5, actuators_tgtStart);

		// Begin heartbeat
		unsigned curPeriod;
		{ Lock l(timingMutex); curPeriod = period; }
		Heartbeat heartbeat(curPeriod);

		// Prepare timing stats
		unsigned long long averagingTicks = 0;
		double frametime=double(curPeriod)/1000000.0, frametimeAvg=0, wakeDevAvg=0,
		       lastFrameBegin = heartbeat.elapsedTime(), lastAvgBegin = lastFrameBegin;
		bool initializing = true;

		// Internal state stuff
		bool lastSineWaveState = sineWave;
		unsigned initSubRL = 0;

		// Button press state stuff
		double lastPos [5] = { std::numeric_limits<double>::infinity() };

		// Main loop
		while (!shutdownRequested())
		{
			////
			// (1) User controls (irrespective of morphling runlevel)

			/* Control period and user frametime */ {
			Lock l(timingMutex);
			curUserFrametime =
				unsigned(userFrametime*1000000.0) > curPeriod ? userFrametime : 0;
			if (setPeriod)
			{
				// Locally duplicate target value to leave critical section ASAP
				curPeriod = period;
				setPeriod = false;
				// Done reading inter-thread state
				l.unlock();

				// Adjust heartbeat
				heartbeat.changePeriod(curPeriod);
			}}

			/* Actuator targets */ {
			Lock l(posTgtMutex);
			// Check if new target requested
			bool newTgt = false;
			for (unsigned i=0; i<5; i++)
				newTgt = newTgt || actuators_tgt[i] != curActuators_tgt[i];
			// Handle new target if requested
			if (newTgt)
			{
				// Reset start point
				for (unsigned i=0; i<5; i++)
					actuators_tgtStart[i] = lerp(
						actuators_tgtStart[i], curActuators_tgt[i], tStep
					);
				tStep = 0;

				// Commit new target positions
				std::copy_n(actuators_tgt, 5, curActuators_tgt);
				/*for (unsigned i = 0; i < 5; i++)
				{
					if (std::abs(actuators_tgt[i] - curActuators_tgt[i]) < 820)
						curActuators_tgt[i] = actuators_tgt[i];
					else
						actuators_tgt[i] = curActuators_tgt[i];
				}*/
			}}


			////
			// (2) Morphling communication

			switch (getRunlevel())
			{
			case RL_POWEROFF: {
				/* Check for initialization request */ {
				Lock l(initMutex);
				if (initialize)
				{
					initialize = false;
					l.unlock();

					{ Lock l(timingMutex);
					  curPeriod = period; }
					heartbeat.reset(curPeriod);
					lastAvgBegin = heartbeat.elapsedTime();
					setRunlevel(RL_INIT);
				}}
			} break;

			case RL_INIT: {
				if (initSubRL == 0)
				{
					// Restart time
					{ Lock l(timingMutex);
					  curPeriod = period; }
					heartbeat.reset(curPeriod);
					lastAvgBegin = heartbeat.elapsedTime();

					// Update sub runlevel
					initSubRL++;
				}
				else if (initSubRL == 1)
				{
					// Wait for 50ms
					if (heartbeat.elapsedTime() > 50)
						initSubRL++;
				}
				else if (initSubRL > 1 && initSubRL < CURRENT_SET_PCOUNT+1)
				{
					// Send currents
					// - peak
					tUDP_Paket_16bit packet/* = {0}*/;
					packet.SBP = SBP_FLAG_NO_OPERATION;
					packet.PA = PAKET_STROMBEGRENZUNG_PEAK;
					packet.laufende_paket_nummer = packetCounter++;
					std::fill_n(packet.daten_16bit, 5, 250);
					packet.checksum =
						checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);
					// - mean (needs to be set every time after peak)
					packet.PA = PAKET_STROMBEGRENZUNG_MEAN;
					packet.laufende_paket_nummer = packetCounter++;
					std::fill_n(packet.daten_16bit, 5, 350);
					packet.checksum =
						checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);

					// Update sub runlevel
					initSubRL++;
				}
				else if (initSubRL == CURRENT_SET_PCOUNT+1)
				{
					// Wait for 50ms
					if (heartbeat.elapsedTime() > 1000)
						initSubRL++;
				}
				else if (initSubRL == CURRENT_SET_PCOUNT+2)
				{
					// Send init command
					tUDP_Paket_16bit packet/* = {0}*/;
					packet.SBP = SBP_FLAG_HERUNTERFAHREN;
					packet.PA = PAKET_REBOOT;
					packet.laufende_paket_nummer = packetCounter++;
					std::fill_n(packet.daten_16bit, 5, 150);
					packet.checksum =
						checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);

					initSubRL++;
				}
				else
				{
					// Done, switch to running mode
					initSubRL = 0;
					setRunlevel(RL_RUNNING);
				}

				/* Check for shutdown request */ {
				Lock l(shutdownMutex);
				if (deinitialize)
				{
					deinitialize = false;
					l.unlock();
					setRunlevel(RL_SHUTDOWN);
				}}
			} break;

			case RL_RUNNING: {
				// ToDo: The details of the "running" state are still ugly, i.e. they
				//       are not implemented as their own individual runlevels

				/* Peak current */ {
				Lock lP(peakMutex), lM(meanMutex);
				if (setPeak < CURRENT_SET_PCOUNT)
				{
					// Locally duplicate target value to leave critical section ASAP
					uint16_t newPeak=peak, curMean=mean;
					setPeak++;
					setMean = setMean<CURRENT_SET_PCOUNT+2 ?
						setMean+1 :
						CURRENT_SET_PCOUNT;
					// Done reading inter-thread state
					lM.unlock(); lP.unlock();

					// Send packet torrent
					// - peak
					tUDP_Paket_16bit packet/* = {0}*/;
					packet.SBP = SBP_FLAG_NO_OPERATION;
					packet.PA = PAKET_STROMBEGRENZUNG_PEAK;
					packet.laufende_paket_nummer = packetCounter++;
					std::fill_n(packet.daten_16bit, 5, newPeak);
					packet.checksum =
						checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);
					// - mean (needs to be set every time after peak)
					packet.PA = PAKET_STROMBEGRENZUNG_MEAN;
					packet.laufende_paket_nummer = packetCounter++;
					std::fill_n(packet.daten_16bit, 5, curMean);
					packet.checksum =
						checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);
				}}

				/* Mean current */ {
				Lock l(meanMutex);
				if (setMean < CURRENT_SET_PCOUNT+2)
				{
					// Locally duplicate target value to leave critical section ASAP
					uint16_t newMean = mean;
					setMean++;
					// Done reading inter-thread state
					l.unlock();

					// Compile packet
					tUDP_Paket_16bit packet/* = {0}*/;
					packet.SBP = SBP_FLAG_NO_OPERATION;
					packet.PA = PAKET_STROMBEGRENZUNG_MEAN;
					packet.laufende_paket_nummer = packetCounter++;
					std::fill_n(packet.daten_16bit, 5, newMean);
					packet.checksum = checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);
				}}

				// Position control
				if (!(setPeak < CURRENT_SET_PCOUNT || setMean < CURRENT_SET_PCOUNT)) {
				if (sineWave)
				{
					double _sineMoveSpeed, _sinePeriodLen;
					{ Lock l(sineMutex);
					  _sineMoveSpeed = sineMoveSpeed; _sinePeriodLen = sinePeriodLen; }
					tUDP_Paket_16bit packet/* = {0}*/;
					packet.SBP = SBP_FLAG_STANDARD_SEND_POS;
					packet.PA = PAKET_DATEN_AKTORPOS;
					packet.laufende_paket_nummer = packetCounter++;
					packet.daten_16bit[0] = sine(sinePos - 65.0);
					packet.daten_16bit[1] = sine(sinePos - 35.0);
					packet.daten_16bit[2] = sine(sinePos);
					packet.daten_16bit[3] = sine(sinePos + 35.0);
					packet.daten_16bit[4] = sine(sinePos + 65.0);
					packet.checksum = checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);

					// Update position
					sinePos += frametime * _sineMoveSpeed;
					if (sinePos > _sinePeriodLen)
						sinePos -= _sinePeriodLen;
					lastSineWaveState = true;
				}
				else if (lastSineWaveState)
				{
					// Indicate completed state change
					lastSineWaveState = false;
					// Stop actuators at current position
					Lock l(posTgtMutex);
					actuators_tgtStart[0] = curActuators_tgt[0] =
						actuators_tgt[0] = sine(sinePos - 65.0);
					actuators_tgtStart[1] = curActuators_tgt[1] =
						actuators_tgt[1] = sine(sinePos - 35.0);
					actuators_tgtStart[2] = curActuators_tgt[2] =
						actuators_tgt[2] = sine(sinePos);
					actuators_tgtStart[3] = curActuators_tgt[3] =
						actuators_tgt[3] = sine(sinePos + 35.0);
					actuators_tgtStart[4] = curActuators_tgt[4] =
						actuators_tgt[4] = sine(sinePos + 65.0);
				}
				else
				{
					tUDP_Paket_16bit packet/* = {0}*/;
					packet.SBP = SBP_FLAG_STANDARD_SEND_POS;
					packet.PA = PAKET_DATEN_AKTORPOS;
					packet.laufende_paket_nummer = packetCounter++;
					if (curUserFrametime > 0 && tStep < 1.0)
					{
						// Advance interpolation step
						tStep = std::min(tStep + (frametime/curUserFrametime), 1.0);

						// Interpolate
						for (unsigned i = 0; i < 5; i++)
							packet.daten_16bit[i] = lerp(
								actuators_tgtStart[i], curActuators_tgt[i], tStep
							);
					}
					else
						std::copy_n(curActuators_tgt, 5, packet.daten_16bit);
					packet.checksum = checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
					udp.send(&packet, UDP_PAKET_16bit_SIZE);
				}}

				/* Check for shutdown request */ {
				Lock l(shutdownMutex);
				if (deinitialize)
				{
					deinitialize = false;
					l.unlock();
					setRunlevel(RL_SHUTDOWN);
				}}
			} break;

			case RL_SHUTDOWN:
				// Gracefully shutdown morphling
				killswitch.disengage();
				setRunlevel(RL_POWEROFF);
				break;

			default:
				// This should not happen...
				throw gcnew System::Exception(
					L"[MorphlingAPI::Comms] FATAL: control thread internal state"
					L" corruption.");
			}


			////
			// (3) Read status messages (irrespective of morphling runlevel)

			tUDP_Paket_16bit packet;
			while (udp.recv(&packet, UDP_PAKET_16bit_SIZE, true))
			{
				// Process them...
				uint32_t check = checksum((uint16_t*)&packet, UDP_PAKET_16bit_SIZE);
				if (check != packet.checksum)
					// We could log this somewhere...
					std::cout << "CHECKSUM MISMATCH!!!" << std::endl;
				else
				{
					// Update current actuator positions
					{ Lock l(posCurMutex);
					  std::copy_n(packet.daten_16bit, 5, actuators_cur); }

					// Button press detection logic
					// - detect state change
					bool allCurBelow = false, allPrevBelow = false;
					for (unsigned i=0; i<5; i++)
					{
						allPrevBelow = allPrevBelow || lastPos[i] < buttonThreshold;
						allCurBelow = allCurBelow || actuators_cur[i] < buttonThreshold;
					}
					// - raise event if changed
					if (!allPrevBelow && allCurBelow)
						if (onMorphlingPressed) onMorphlingPressed->pressed();
					// - update state
					std::copy_n(actuators_cur, 5, lastPos);
				}
			}


			////
			// (4) Control timing

			// Wait for next tick
			heartbeat.wait();

			// Timekeeping
			double newTick = heartbeat.elapsedTime();
			frametime = (newTick - lastFrameBegin) / 1000.0; // frametime is in seconds
			lastFrameBegin = newTick;

			// Timing stats
			frametimeAvg += frametime;
			wakeDevAvg += heartbeat.lastWakeDeviation();
			averagingTicks++;
			if (newTick - lastAvgBegin > 1000.0)
			{
				frametimeAvg /= double(averagingTicks);
				wakeDevAvg /= double(averagingTicks);
				std::cout
					<< std::setprecision(7)
					<< "--------------" << std::endl
					<< " [M] tickcount = "<<heartbeat.elapsedBeats()<<", " << averagingTicks
						<< "tps  (missed: "<<heartbeat.skippedBeats()<<")" << std::endl
					<< " [M] tick time = " << frametimeAvg*1000 << "ms" << std::endl
					<< " [M] wake dev. = "<< heartbeat.lastWakeDeviation() <<"ms" << std::endl;
				lastAvgBegin = lastFrameBegin;
				averagingTicks = 0;
				frametimeAvg = 0;
				wakeDevAvg = 0;
			}
		}

		// Try and shut down the morphling gracefully before exiting
		killswitch.disengage();

		// Done! Nominal exit...
		return 0;
	}
};



//////
//
// Class implementation
//

////
// MorphlingComms

#define MORPHCOM_IMPL ((MorphlingThread*)pimpl)

MorphlingComms::MorphlingPressedCallback::~MorphlingPressedCallback() {}

MorphlingComms::MorphlingComms(const void* serverAddress, unsigned port)
	: pimpl(nullptr)
{
	// Allocate implementation instance
	pimpl = new MorphlingThread((const TCHAR*)serverAddress, port);
	if (pimpl == nullptr)
		throw gcnew System::OutOfMemoryException(L"[MorphlingAPI::Comms] Failed to"
		                                         L"allocate class instance.");

	// Check if realtime environment is available
	double minTimerPeriod = Heartbeat::getMinPossibleTimerPeriod();
	if (minTimerPeriod > 500)
		throw gcnew System::NotSupportedException(formatManagedExceptionMsg(
			L"MorphlingAPI::Comms", L"Insufficient system realtime capabilities - timer"
			L" resolution of 500 microseconds or less required.",
			{{ L"minTimerPeriod", minTimerPeriod }}
		));

	// Start communications thread
	MORPHCOM_IMPL->start();
}

MorphlingComms::~MorphlingComms()
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	if (impl)
	{
		impl->shutdown();
		delete impl;
		pimpl = nullptr;
	}
}

void MorphlingComms::setHeight (unsigned actuator, double height)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(MORPHCOM_IMPL->posTgtMutex);
	impl->actuators_tgt[actuator] =
		  MIN_VALUE_16Bit_POS
		+ unsigned(
		  	double(MAX_VALUE_16Bit_POS-MIN_VALUE_16Bit_POS)*clamp(height, 3.0, 76.5)/80.0
		  );
}

void MorphlingComms::setHeights (
	const std::vector<unsigned> &IDs, const std::vector<double> &heights
)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(MORPHCOM_IMPL->posTgtMutex);
	for (unsigned i=0; i<IDs.size(); i++)
		impl->actuators_tgt[IDs[i]] =
			  MIN_VALUE_16Bit_POS
			+ unsigned(
			  	  double(MAX_VALUE_16Bit_POS - MIN_VALUE_16Bit_POS)
			  	* clamp(heights[i], 3.0, 76.5)/80.0
			  );
}

void MorphlingComms::setPose (const std::vector<double> &pose)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(MORPHCOM_IMPL->posTgtMutex);
	for (unsigned i=0; i<5; i++)
		impl->actuators_tgt[i] =
			  MIN_VALUE_16Bit_POS
			+ unsigned(
			  	  double(MAX_VALUE_16Bit_POS - MIN_VALUE_16Bit_POS)
			  	* clamp(pose[i], 3.0, 76.5)/80.0
			  );
}

void MorphlingComms::getPose (std::vector<double> *pose_out)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(impl->posCurMutex);
	pose_out->resize(5);
	for (unsigned i=0; i<5; i++)
		pose_out->at(i) =
			  (impl->actuators_cur[i]-double(MIN_VALUE_16Bit_POS))*80.0
			/ double(MAX_VALUE_16Bit_POS-MIN_VALUE_16Bit_POS);
}

void MorphlingComms::setMorphlingPressedCallback (MorphlingPressedCallback &callback)
{
	MORPHCOM_IMPL->onMorphlingPressed = &callback;
}

void MorphlingComms::setMorphlingPressedThreshold (double height)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(impl->buttonThreshMutex);
	impl->buttonThreshold = height;
}

void MorphlingComms::setPeakCurrent (double value)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(impl->peakMutex);
	impl->peak = unsigned(value*1000);
	impl->setPeak = 0;
}

void MorphlingComms::setMeanCurrent (double value)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(impl->meanMutex);
	impl->mean = unsigned(value * 1000);
	impl->setMean = 0;
}

void MorphlingComms::setControlPeriod (unsigned period)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(impl->timingMutex);
	impl->period = period;
	impl->setPeriod = true;
}

void MorphlingComms::setFrametime(double frametime)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(impl->timingMutex);
	impl->userFrametime = frametime;
}

void MorphlingComms::startSineWave (void)
{
	MORPHCOM_IMPL->sineWave = true;
}

void MorphlingComms::stopSineWave(void)
{
	MORPHCOM_IMPL->sineWave = false;
}

void  MorphlingComms::setSineWaveParams (double periodLength, double movementSpeed)
{
	// Shortcut to save one indirection per member access
	register MorphlingThread *impl = MORPHCOM_IMPL;

	// Send new value
	Lock l(impl->sineMutex);
	impl->sinePeriodLen = periodLength;
	impl->sineMoveSpeed = movementSpeed;
}

void MorphlingComms::startup (bool block)
{
	MORPHCOM_IMPL->startup();
}

void MorphlingComms::shutdown (void)
{
	MORPHCOM_IMPL->powerdown();
}



//////
//
// Class implementation
//

////
// RobotArmComms

struct RobotThread : public Thread
{
	TCP tcp;

	// Task messaging
	// ToDo: this is of general usefulness, encapsulate in its own class or even module
	struct Task
	{
		enum Type {
			RTT_PING, RTT_EXTEND, RTT_RETRACT, RTT_CFGRELOAD, RTT_STARTCTRL, RTT_STOPCTRL
		} type;
		unsigned param;
		bool success;
		Event done;
		RobotArmComms::RobotArmOperationCallback *callback;

		Task(Type type, RobotArmComms::RobotArmOperationCallback *callback)
			: type(type), callback(callback)
		{}
	};
	Mutex taskMutex;
	std::queue<std::shared_ptr<Task> > taskQueue;

	RobotArmComms::RobotArmOperationCallback *onRobotArmOpComplete;

	RobotThread(const TCHAR *address, unsigned port)
		: tcp(IPTransport::resolveHost(address), port),
		  taskMutex(L"RobotThread_task__" + nameFromPointer(this)),
		  onRobotArmOpComplete(nullptr)
	{
	}

	Task& queueTask(const std::shared_ptr<Task> &task)
	{
		Lock l(taskMutex);
		taskQueue.push(task);
		return *(taskQueue.back());
	}

	unsigned run(void)
	{
		// Begin heartbeat
		Heartbeat heartbeat(50000);

		// Prepare timing stats
		unsigned long long averagingTicks = 0;
		double frametime, frametimeAvg=0, wakeDevAvg=0,
		       lastFrameBegin = heartbeat.elapsedTime(), lastAvgBegin = lastFrameBegin;

		// Internal state stuff
		unsigned runlevel = 0;

		// Arm ext/retr state
		std::chrono::high_resolution_clock::time_point ra_begin;


		// Main loop
		std::shared_ptr<Task> task;
		while (!shutdownRequested())
		{
			////
			// Message handling

			// Fetch a task
			if (!task)
			{
				Lock l(taskMutex);
				if (!taskQueue.empty())
				{
					task = taskQueue.front();
					taskQueue.pop();
					l.unlock();
					runlevel = 0;
				}
			}

			// Perform the task if one was queued
			if (task)
			{
				switch (task->type)
				{
				case Task::RTT_PING:
					if (runlevel == 0)
					{
						// Send message
						std::string cmd("1;");
						tcp.send(cmd.c_str(), (unsigned)cmd.length());
						ra_begin = std::chrono::high_resolution_clock::now();
						runlevel = 1;
					}
					else
					{
						// Wait for ping-back but time out after requested number of seconds
						if (  std::chrono::high_resolution_clock::now() - ra_begin
						    < std::chrono::nanoseconds(task->param*1000000ULL))
						{
							char response [3]; std::fill_n(response, 3, 0);
							if (tcp.recv(response, 3, true))
								if (response[0] == '1' && response[1] == ';')
								{
									task->success = true;
									task->done.signal();
									task.reset();
								}
						}
						else
						{
							task->success = false;
							task->done.signal();
							task.reset();
						}
					}
					break;

				case Task::RTT_EXTEND:
				case Task::RTT_RETRACT:
					if (runlevel == 0)
					{
						// Send message
						std::string cmd(task->type == Task::RTT_EXTEND ? "2,1;" : "2,2;");
						tcp.send(cmd.c_str(), (unsigned)cmd.length());
						ra_begin = std::chrono::high_resolution_clock::now();
						runlevel = 1;
					}
					else
					{
						// Wait for confirmation but time out after 30 seconds
						if (  std::chrono::high_resolution_clock::now() - ra_begin
						    < std::chrono::nanoseconds(30000000000))
						{
							char response [3]; std::fill_n(response, 3, 0);
							if (tcp.recv(response, 3, true))
								if (response[0] == '3' && response[1] == ';')
								{
									task->success = true;
									if (task->callback)
										task->callback->done(
											task->type == Task::RTT_EXTEND ?
												RobotArmComms::RA_EXTEND :
												RobotArmComms::RA_RETRACT,
											task->success
										);
									task->done.signal();
									task.reset();
								}
						}
						else
						{
							task->success = false;
							if (task->callback)
								task->callback->done(
									task->type == Task::RTT_EXTEND ?
										RobotArmComms::RA_EXTEND :
										RobotArmComms::RA_RETRACT,
									task->success
								);
							task->done.signal();
							task.reset();
						}
					}
					break;

				case Task::RTT_CFGRELOAD:
					// Not currently handled in-thread
					*((int*)nullptr) = 0;

				case Task::RTT_STARTCTRL:
				case Task::RTT_STOPCTRL:
					// Not currently handled in-thread
					*((int*)nullptr) = 0;
				}
			}


			////
			// Control timing

			// Wait for next tick
			heartbeat.wait();

			// Timekeeping
			double newTick = heartbeat.elapsedTime();
			frametime = (newTick - lastFrameBegin) / 1000.0; // frametime is in seconds
			lastFrameBegin = newTick;

			// Timing stats
			frametimeAvg += frametime;
			wakeDevAvg += heartbeat.lastWakeDeviation();
			averagingTicks++;
			if (newTick - lastAvgBegin > 1000.0)
			{
				frametimeAvg /= double(averagingTicks);
				wakeDevAvg /= double(averagingTicks);
				std::cout
					<< std::setprecision(7)
					<< "--------------" << std::endl
					<< " [R] tickcount = "<<heartbeat.elapsedBeats()<<", " << averagingTicks
						<< "tps  (missed: "<<heartbeat.skippedBeats()<<")" << std::endl
					<< " [R] tick time = " << frametimeAvg*1000 << "ms" << std::endl
					<< " [R] wake dev. = "<< heartbeat.lastWakeDeviation() <<"ms" << std::endl;
				lastAvgBegin = lastFrameBegin;
				averagingTicks = 0;
				frametimeAvg = 0;
				wakeDevAvg = 0;
			}
		}

		// Done! Nominal exit...
		return 0;
	}
};
#define ROBOCOM_IMPL ((RobotThread*)pimpl)

RobotArmComms::RobotArmOperationCallback::~RobotArmOperationCallback() {}

RobotArmComms::RobotArmComms(const void *address, unsigned port) : pimpl(nullptr)
{
	// Allocate implementation instance
	pimpl = new RobotThread((const TCHAR*)address, port);
	if (pimpl == nullptr)
		throw gcnew System::OutOfMemoryException(L"[MorphlingAPI::RobotArm] Failed to"
		                                         L" allocate class instance.");
	ROBOCOM_IMPL->start();
}

RobotArmComms::~RobotArmComms()
{
	if (pimpl)
	{
		ROBOCOM_IMPL->shutdown();
		delete ROBOCOM_IMPL;
		pimpl = nullptr;
	}
}

bool RobotArmComms::ping (unsigned timeout)
{
	std::shared_ptr<RobotThread::Task> ping(new RobotThread::Task(
		RobotThread::Task::RTT_PING, nullptr
	));
	ping->param = timeout ? timeout : 5000;
	ROBOCOM_IMPL->queueTask(ping);
	ping->done.wait(0);
	return ping->success;
}

void RobotArmComms::extend (bool block)
{
	std::shared_ptr<RobotThread::Task> extend(new RobotThread::Task(
		RobotThread::Task::RTT_EXTEND, ROBOCOM_IMPL->onRobotArmOpComplete
	));
	ROBOCOM_IMPL->queueTask(extend);
	if (block)
		extend->done.wait(0);
}

void RobotArmComms::retract (bool block)
{
	std::shared_ptr<RobotThread::Task> retract(new RobotThread::Task(
		RobotThread::Task::RTT_RETRACT, ROBOCOM_IMPL->onRobotArmOpComplete
	));
	ROBOCOM_IMPL->queueTask(retract);
	if (block)
		retract->done.wait(0);
}

void RobotArmComms::reloadConfig (void)
{
	// This is fire-and-forget according to protocol, so don't bother the thread with it
	std::string cmd("4;");
	ROBOCOM_IMPL->tcp.send(cmd.c_str(), (unsigned)cmd.length());
}

void RobotArmComms::enableControl (bool enabled)
{
	// This is fire-and-forget according to protocol, so don't bother the thread with it
	std::string cmd(enabled ? "5,1;" : "5,2;");
	ROBOCOM_IMPL->tcp.send(cmd.c_str(), (unsigned)cmd.length());
}

void RobotArmComms::setOperationCompleteCallback (RobotArmOperationCallback &callback)
{
	ROBOCOM_IMPL->onRobotArmOpComplete = &callback;
}
