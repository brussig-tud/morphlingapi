
#ifndef __UTILS_H__
#define __UTILS_H__


//////
//
// Namespaces
//

// Implemented namespace
namespace Morphling {



//////
//
// Typedefs
//


#if (_MSC_VER < 1910)
	/** @brief A functional @c any implementation. */
	typedef boost::any Any;

	/** @brief A functional @c anyCast implementation. */
	template <class T>
	inline T* anyCast (const Any &container) { return (T*)boost::any_cast<T>(&container); }
#else
	/** @brief A functional @c any implementation. */
	typedef std::any Any;

	/** @brief A functional @c anyCast implementation. */
	template <class T>
	inline T* anyCast(const Any &container) { return (T*)std::any_cast<T>(&container); }
#endif

/** @brief STL string type with appropriate character width. */
typedef std::basic_string<TCHAR> String;

/**
 * @brief
 *		STL pair representing the name of a variable and its value. Used in conjunction
 *		with the error message formatting utilities.
 */
typedef std::pair<String, Any> VarValuePair;

/**
 * @brief
 *		List of variable-value pairs. Used in conjunction with the error message
 *		formatting utilities.
 */
typedef std::vector<VarValuePair> VarValueList;



//////
//
// Functions
//

/**
 * @brief Returns a system-defined description string for the given error code.
 *
 * @param errorCode
 *		The error code to try and find a description for. Will only work with error codes
 *		defined by the Windows SDK.
 */
String getSystemErrorMsg (unsigned errorCode);

/**
 * @brief Formats a generic error message suitable for decorating exceptions.
 *
 * This funciton is useful when the error message is supposed to give information about
 * the contents of variables associated with the error. Otherwise, generic string
 * operations might be a better way to compose the error message.
 *
 * @param moduleDesc
 *		Relatively short string identifying the functional unit the error occured in.
 * @param errorDesc
 *		As-long-as-necessary text describing the error that occured.
 * @param params
 *		An optional list of variable-value pairs relevant to the error.
 */
String formatExceptionMsg (
	const String &moduleDesc, const String &errorDesc,
	const VarValueList &params=VarValueList()
);

/**
 * @brief
 *		Formats an error message suitable for decorating exceptions that relate to failed
 *		Windows SDK calls.
 *
 * In addition to formatting the error message like @ref
 * formatExceptionMsg(const String&, const String&, const VarValueList&) , this function
 * also attempts to add a description of the Windows SDK error indicated by @c errorCode
 * as the @e Reason for the error.
 *
 * @param errorCode
 *		The error code to try and find a description for. Will only work with error codes
 *		defined by the Windows SDK.
 * @param moduleDesc
 *		Relatively short string identifying the functional unit the error occured in.
 * @param errorDesc
 *		As-long-as-necessary text describing the error that occured.
 * @param params
 *		An optional list of variable-value pairs relevant to the error.
 */
String formatExceptionMsg (
	unsigned errorCode, const String &moduleDesc, const String &errorDesc,
	const VarValueList &params=VarValueList()
);

/**
 * @brief
 *		Formats a generic error message suitable for decorating exceptions, returning the
 *		error message as a managed CLR <tt>System.String</tt> object.
 *
 * This funciton is useful when the error message is supposed to give information about
 * the contents of variables associated with the error. Otherwise, generic string
 * operations might be a better way to compose the error message.
 *
 * @param moduleDesc
 *		Relatively short string identifying the functional unit the error occured in.
 * @param errorDesc
 *		As-long-as-necessary text describing the error that occured.
 * @param params
 *		An optional list of variable-value pairs relevant to the error.
 */
System::String^ formatManagedExceptionMsg (
	const String &moduleDesc, const String &errorDesc,
	const VarValueList &params=VarValueList()
);

/**
 * @brief
 *		Formats an error message suitable for decorating exceptions that relate to failed
 *		Windows SDK calls. Returns the error message as a managed CLR
 *		<tt>System.String</tt> object.
 *
 * In addition to formatting the error message like @ref
 * formatManagedExceptionMsg(const String&, const String&, const VarValueList&) , this
 * function also attempts to add a description of the Windows SDK error indicated by @c
 * errorCode as the @e Reason for the error.
 *
 * @param errorCode
 *		The error code to try and find a description for. Will only work with error codes
 *		defined by the Windows SDK.
 * @param moduleDesc
 *		Relatively short string identifying the functional unit the error occured in.
 * @param errorDesc
 *		As-long-as-necessary text describing the error that occured.
 * @param params
 *		An optional list of variable-value pairs relevant to the error.
 */
System::String^ formatManagedExceptionMsg (
	unsigned errorCode, const String &moduleDesc, const String &errorDesc,
	const VarValueList &params=VarValueList()
);

/**
 * @brief
 *		Formats an error message suitable for decorating exceptions that relate to an
 *		inner exception containing the root cause of the error. Returns the error message
 *		as a managed CLR <tt>System.String</tt> object.
 *
 * In addition to formatting the error message like @ref
 * formatManagedExceptionMsg(const String&, const String&, const VarValueList&) , this
 * function also adds a description of the given inner exception as the @e Reason for the
 * error.
 *
 * @param inner
 *		The root cause of this error.
 * @param moduleDesc
 *		Relatively short string identifying the functional unit the error occured in.
 * @param errorDesc
 *		As-long-as-necessary text describing the error that occured.
 * @param params
 *		An optional list of variable-value pairs relevant to the error.
 */
System::String^ formatManagedExceptionMsg (
	System::Exception^ inner, const String &moduleDesc, const String &errorDesc,
	const VarValueList &params=VarValueList()
);

/**
 * @brief Clamps the argument to the given interval.
 *
 * @param value
 *		The value to be clamped.
 * @param low
 *		The lower bound of the clamp interval.
 * @param high
 *		The upper bound of the clamp interval.
 */
template <class T>
inline T clamp (T value, T low, T high)
{
	T _min = value < high ? value : high;
	return _min > low ? _min : low;
}



//////
//
// Classes
//

/**
 * @brief
 *		RAII wrapper to make dealing with C-style handles and similar things that are
 *		hard to manage using STL-provided facilites (like unique_ptr) exception safe.
 */
template <
	class T, class finalizer_rettype, class finalizer_argtype,
	finalizer_rettype (*finalizer)(finalizer_argtype*)
>
struct GenericRAII
{
	////
	// Data members

	/** @brief The C-style thing (handle, object etc.) managed by this RAII-wrapper. */
	T *thing;


	////
	// Object construction / destruction

	/** @brief The default constructor. */
	inline GenericRAII() : thing(nullptr)
	{};

	/**
	 * @brief Constructs the RAII wrapper with the given @ref #thing to manage.
	 *
	 * @param thing
	 *		The C-style thing (handle, object etc.) that should be made exception-safe
	 *		via RAII.
	 */
	inline GenericRAII(T *thing) : thing(thing)
	{};

	/** @brief The destructor. */
	~GenericRAII()
	{
		if (thing)
		{
			finalizer(thing);
			thing = nullptr;
		}
	}


	////
	// Conversion

	/** @brief Access to managed thing as C-style pointer. */
	inline operator T* (void) { return thing; }

	/** @brief Dereference to managed thing. */
	inline T& operator * (void) { return *thing; }

	/**
	 * @brief
	 *		Get the memory address of the thing (in many cases that will yield a
	 *		pointer-to-a-pointer).
	 */
	inline T** address (void) { return &thing; }
};

/**
 * @brief
 *		Specialized version of @ref GenericRAII suitable for most C-style objects (with
 *		the notable exception of memory buffers allocated using @c malloc - use @ref
 *		RAIIbuffer for that). It assumes a finalizer with @c void return type and an
 *		argument of the same type as the @ref GenericRAII::thing .
 */
template <class T, void (*finalizer)(T*)>
using RAII = GenericRAII<T, void, T, finalizer>;

/**
* @brief
*		Specialized version of @ref GenericRAII suitable for C-style memory buffers like
*		dynamic arrays allocated with @c malloc . It assumes a finalizer with @c void
*		return type taking a @c void pointer as argument.
*/
template <class T, void (*finalizer)(void*)>
using RAIIbuffer = GenericRAII<T, void, void, finalizer>;


// Close implemented namespace
}


#endif // ifndef __UTILS_H__
