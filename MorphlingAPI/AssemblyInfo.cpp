
using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::InteropServices;

[assembly:AssemblyTitleAttribute(L"MorphlingAPI")];
[assembly:AssemblyDescriptionAttribute(L"C++ and CLR/.NET library for controlling the"
                                       L"Fast Haptic morphling device.")];
[assembly:AssemblyConfigurationAttribute(L"")];
[assembly:AssemblyCompanyAttribute(L"TU Dresden")];
[assembly:AssemblyProductAttribute(L"MorphlingAPI")];
[assembly:AssemblyCopyrightAttribute(L"Copyright (c)  2019")];
[assembly:AssemblyVersionAttribute("0.3.0")];

[assembly:ComVisible(true)];
[assembly:CLSCompliantAttribute(true)];
