#include <stdint.h>
#include <math.h>       /* floor */
//#include <arduino.h> //Serial.println("text");
#include "kommunikation_protokoll_invhand_v11.h"

//Timing:
extern "C" const uint16_t delay_UDP_schreiben = 5000; //in Mikrosekunden //ms
//der Lesezugriff wurde an den Schreibzugriff angehaengt, damit entfaellt die extra Lese-Ueberpruefung.
extern "C" const uint16_t delay_UDP_lesen = 5000; //in Mikrosekunden 


/*
 * FUNKTIONEN zum EMPFANGEN von UDP Paket mit Inhalt ;-):
 */

/* ....siehe Hauptprogramm ...
    //**********************************************************
    // ******** UDP Protokoll Funktionen und Verfahren     *****
    //**********************************************************
       uint8_t statusbit_paket=0;
       uint16_t array_ist_positionen[5];
       tDatenInvHand daten_aktuell; 


uint8_t UDP_get_buffer(char* _UDP_buffer, uint8_t _size_buffer)
{
  // if there's data available ...
  if(Udp.available())
  {
     int packetSize = Udp.parsePacket();    //Groesse checken
     if (packetSize) //d.h. Groesse > 0
       if (packetSize<UDP_PAKET_16bit_SIZE) //zu klein fuer vollstaendiges Paket
       {
         Udp.read(_UDP_buffer, _size_buffer); //UDP Stack leeren und Daten weghauen
         return 0; //kein ganzes Paket am stueck erhalten
       }
       else
       {
            IPAddress  remote = Udp.remoteIP(); //Sender IP Adresse
            // read the packet into packetBufffer
            Udp.read(_UDP_buffer, _size_buffer); //UDP_PACKET_SIZE_BYTES

            //number_packets_received=packetSize/UDP_PAKET_16bit_SIZE;  //Berechnen, wie viele Pakete am Stueck angekommen sind
            //return number_packets_received;
            return packetSize/UDP_PAKET_16bit_SIZE;  //Berechnen, wie viele Pakete am Stueck angekommen sind
       }
  } 
  return 0; 
}

/**/

/*
 * Bildet eine Checksumme aus Inhalt in uebergebenem Pointer
 * @param f_buffer Pointer auf Inhalt
 * @return Checksumme im Format ??
 */  
extern "C" uint32_t checksum(uint16_t* _buffer, uint8_t _number_int16_t) //tDatenInvHand daten_struktur
{
  uint32_t tmp_sum=0;
  for(uint8_t i=2;i<floor(_number_int16_t/2);i++) //i=0..1 enthaelt die Checksumme, wird deshalb ausgelassen. Checksumme kommt zuletzt rein
  {
    tmp_sum +=  (uint32_t)_buffer[i]; //_cp_to_daten_struktur[i-2] =
  }
  
  return tmp_sum;
};

/*
 * Kopiert den Lese-Buffer in einem zu bearbeitenden Datenfeld und bildet die Checksumme
 */
extern "C" uint32_t cp_checksum(uint16_t* _buffer, uint8_t _number_int16_t, uint16_t* _cp_to_daten_struktur) //tDatenInvHand daten_struktur
{
  uint32_t tmp_sum=0;
  for(uint8_t i=2;i<floor(_number_int16_t/2);i++) //i=0..1 enthaelt die Checksumme, wird deshalb ausgelassen. Checksumme kommt zuletzt rein
  {
    tmp_sum +=  _cp_to_daten_struktur[i-2] =(uint32_t)_buffer[i]; // Daten an Zielposition kopieren und anschliessend zur Summe hinzufuegen
  }
  
  return tmp_sum;
};

//Kopiert den UDP-Lese-Buffer in einem Datenfeld vom Typ tDatenInvHand daten_struktur
//_buffer: UDP read Buffer; _number_int16_t Groesse des Buffers [als Anzahl von uint16_t]; 
//daten_struktur: Aufbewahrungsort ausserhalb des Lese_buffers 
extern "C" void cp_daten(uint16_t* _buffer, uint8_t _number_int16_t, uint16_t* daten_struktur)
{
  int i;
  
  for(i=2;i<floor(_number_int16_t/2);i++) //i=0..1 enthaelt die Checksumme, wird deshalb ausgelassen. Checksumme kommt zuletzt rein
  {
    daten_struktur[i-2] = _buffer[i]; 
  }
};
