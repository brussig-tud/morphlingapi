//Datei: kommunikation_protokoll_invhand_v11.h

//-> Vorsicht .. Version trotzt gleicher Bezeichnung bei Sender und Empfaenger ggf. unterschiedlich
/* PaketInversHandschuh.h ********************
***** zur Kommunikation über UDP Protokoll ***
*********************************************/

#ifndef PaketInversHandschuh  //Mehrfachaufruf möglich ohne Fehlermeldung
#define PaketInversHandschuh

//Timing:
#define DELAY_UNIT 0.000001 // 1 unit = 1 microsecond
extern "C" const uint16_t delay_UDP_schreiben; //in DELAY_UNITs
//der Lesezugriff wurde an den Schreibzugriff angehaengt, damit entfaellt die extra Lese-Ueberpruefung.
extern "C" const uint16_t delay_UDP_lesen; //in DELAY_UNITs


//Konstanten fuer die AktorHubBerechnung
//80000um entspricht uint16_t VariablenWert 13332
#define MAX_POS_um 80000  //Aktorposition in Mikrometer
#define MIN_POS_um     0
#define MAX_VALUE_16Bit_POS 13332 //Entspricht 80mm Position
#define MIN_VALUE_16Bit_POS 200 //Entspricht 0mm Position
#define HOME_POS 150
#define VAL2POS 6    // Konvertierungsfaktor zw. Position und uint16_t Variable 80001/13333=6 
#define Transfer2Faulhaber 6666   //Faulhaber Koordinaten = Positionsdaten - TransferToFaulhaber

//Konstanten PC-Seite
#define MAX_VALUE_16Bit_POS_PC 12748  // 76,5mm

//Definition fuer UDP Pakete mit 16Bit Daten im Datenbereich <PAKET16>:

//UDP (Schreib-Buffer) Groesse:
#define UDP_PAKET_16bit_SIZE 18 
#define UDP_READ_BUFFER_RESERVED 10*UDP_PAKET_16bit_SIZE  //bis zu 10 Pakete sollen im Read Buffer platz haben 

//Datenstruktur des Pakets fuer die Senderoutine

struct tUDP_Paket_16bit
{
      //Pos0:
      uint32_t checksum=0;
      //Pos4:
      uint8_t SBP=0; //StatusBits des UDP Pakets siehe unten
      //Pos5:
      uint8_t PA=0; //PaketArt bzw. DatenArt ... die unten im Datenbereich kommt
      //Pos6:
      uint8_t LDP_RD=0; //Letztes Dekodiertes Paket (die zuletzt dekodierte Paketart) Wert 0x0000, falls ein Fehler bei Dekodierung
                        // oder RD ..Return Type Of Data d.h. Sender will von InvHandsch im Antwortpaket Ist-Position oder Strom oder ...
      //Pos7:
      uint8_t laufende_paket_nummer=0; //Der Sender inkrementiert diese Nummer, .. der Empfaenger..InvHandsch gibt die Nummer und die dekodierte Paketart, falls erforlgreich
      //Pos8:
      uint16_t daten_16bit[5];
      char* Nullerminierung = '\0';     
};


struct tDatenInvHand
{
      //Pos0:
      uint8_t SBP=0; //StatusBits des UDP Pakets siehe unten
      //Pos1:
      uint8_t PA=0; //PaketArt bzw. DatenArt ... die unten im Datenbereich kommt
      //Pos2:
      uint8_t LDP_RD=0; //Letztes Dekodiertes Paket (die zuletzt dekodierte Paketart) Wert 0x0000, falls ein Fehler bei Dekodierung
                        // oder RD ..Return Type Of Data d.h. Sender will von InvHandsch im Antwortpaket Ist-Position oder Strom oder ...
      //Pos3:
      uint8_t laufende_paket_nummer=0; //Der Sender inkrementiert diese Nummer, .. der Empfaenger..InvHandsch gibt die Nummer und die dekodierte Paketart, falls erforlgreich
      //Pos4-13:
      uint16_t daten_16bit[5]; 
      //pos14:
      uint8_t daten_sind_gueltig=0; //Info, ob das Paket valide ist
      uint16_t zeitstempel; //z.B. ein interner Counter im jeweiligen Controller
};
    

/*******
* Array PaketArt PA (Pos5): 1x uint8_t Paketart(Datenart):
*********************************************
PaketArt (uint8_t) Art des Paketinhaltes, Inhalt der uint8_t Zahl: 
********
PaketKennzeichnung, Erklärung, Inhalt der Daten:
/**/
//0, Nicht dekodierbares Paket (Fehlerinfo vom Empfänger) 
#define PAKET_NOT_DECODED 0x00    //Fehlermeldung von InvHandsch an Sender
//1, Aktorposition-Paket, (Daten im uint16_t Format, Soll-Aktorposition)
#define PAKET_DATEN_AKTORPOS  1 
//2, Stromstaerke, Peak (Daten im uint16_t Format, Soll-Stromstaerke) 
#define PAKET_STROMBEGRENZUNG_PEAK 2
//4, Stromstaerke, MeanVal (Daten im uint16_t Format, Soll-Stromstaerke) 
#define PAKET_STROMBEGRENZUNG_MEAN 4
//80, Init        (Daten im uint16_t Format) zusammen mit SBP=SBP_FLAG_INIT
//                 Datenteil des Pakets enthalten die Soll-Stromstärke(mean-value)      
#define PAKET_INIT 80
//160, Shutdown,  (Daten im uint16_t Format)
//                 Datenteil des Pakets enthalten die Soll-Stromstärke(mean-value)      
#define PAKET_SHUTDOWN 160 //zusammen mit SBP=SBP_FLAG_HERUNTERFAHREN     
#define PAKET_REBOOT 240 //zusammen mit SBP=SBP_FLAG_HERUNTERFAHREN

/*************************************
*   STATUSBITS:                      * 
**************************************/
/* Bedeutung der einzelnen Statusbits Bitpositionen innerhalb des uint8_t:
**
SBP = StatusBit des Paketes
SBS = StatusBit des Systems Inverser Handschuh
*/

/** *******************************************
*  ... Sender an InvHand SBP Strukturen:       *
***********************************************/
//0b 0000 0000: Betriebszustand, betriebsbereit (Positionsdaten empfangen und zu Sollposition hingehen)
#define SBP_FLAG_STANDARD_SEND_POS   0b00000000 //Standard-Betriebszustand ..im Positionsmodus
//0b 0000 0001: emmergency (Wert auf 1 -> emmergency stop, nicht bewegen)
#define SBP_FLAG_EMMERGENCY      0b00000001 //0 alles i.O. 1 HALT und nicht weiter bewegen
//0b 0000 0010: (Sender an InvHandsch:) jetzt Herunterfahren -> Wert auf 1 + zusaetzlich Paketart PAKET_SHUTDOWN 
#define SBP_FLAG_HERUNTERFAHREN  0b00000010 // Befehl System herunterfahren
//0b 0000 0100: Init (Wert auf 1: System neu initiieren)
#define SBP_FLAG_INIT            0b00000100 // Befehl Init durchfuehren
//0b 0000 1000: Befehl "No Operation" d.h..Laufende Operation weitermachen 
//              und den Systemzustand (Statusbits) zurueckliefern und
//              ggf. Stromstaerke aendern mit Spezialbefehl fuer Stromstaerke
#define SBP_FLAG_NO_OPERATION    0b00001000 // Befehl <ZustandReport + Aktuellen Prozess weitermachen>

/** SBS .. StatusBits des Systems InvHandsch
* StatusBits: InvHand an Sender:
************************************/
//Befehlspart der Statusbits, hier steckt Info, welcher Befehl aktuell ausgefuehrt wird
//0b 0000 0000: Betriebszustand, betriebsbereit (Positionsdaten empfangen und zu Sollposition hingehen)
// -> normaler Betrieb ohne Flag-Beruecksichtigung
#define SYS_STANDARD_POS          0b0000 0000 //Standard-Betriebszustand ..im Positionsmodus
#define SYS_FLAG_EMMERGENCY       0b0000 0001 //an: Befehl <HALT und nicht bewegen> wird ausgefuehrt
#define SYS_FLAG_HERUNTERFAHREN   0b0000 0010 // an: Befehl <Herunterfahren> wird ausgefuehrt
#define SYS_FLAG_INIT             0b0000 0100 // 0 Betriebszustand, 1 Befehl <Init> wird ausgefuehrt
#define SYS_FLAG_NO_OPERATION     0b0000 1000 // 0 Betriebszustand, 1 Befehl <Aktuellen Prozess zu ende machen, keine Befehlsentgegennahme>

//Systemzustand-Part der Statusbits, hier steckt Info, ob die Operation noch laeuft oder fertig ist oder andere Fehler vorliegen
//0b 0001 0000  an:(emmergency stop .. einer der Controller kommuniziert nicht oder Ueberhitzung)
#define SYS_FLAG_SYSTEM_FEHLER      0b0001 0000 //1 Systemfehler ... z.B. Kommunikation mit einem (oder mehr) Faulhaber Controller klappt nicht
//0b 0010 0000  an: System befindet sich im zusammengeklappten Zustand (reduzierte Stromstaerke)
#define SYS_FLAG_SYSTEM_EINGEKLAPPT  0b0010 0000 //0 Betriebsbereit; 1 System ist eingeklappt (heruntergefahren)
//0b 0100 0000  an: es wird gerade ein Zustandswechsel durchgefuehrt 
#define SYS_FLAG_ZUSTANDS_WECHSEL    0b0100 0000 //0 Normalbetrieb; 1 Zustandwechsel wird durchgefuehrt

#define UDP_PORT 8888 //Der Port fuer die Kommunikation mit InversHandschuh
//byte mac_win_pc[] = {0x90, 0xA2, 0xDA, 0x10, 0xD1, 0xFE};  //Sender - Mikrokontroller fungiert auch als WinPC (Simuliert es)
//IPAddress ip_win_pc(192, 168, 0, 26);        //WIN PC with 3D Graphics, ist der Client

//byte mac_InvHand[] = {0x90, 0xA2, 0xDA, 0x10, 0xB4, 0x1A};
//IPAddress ip_inv_hand(192, 168, 0, 100);  //controller of InvHandsch, ist der Server 

/**********************************************
 * 
 *   Definition der Funktionen      ***********
 *                                          ***
 **********************************************/

uint8_t UDP_get_buffer(char* _UDP_buffer, uint8_t _size_buffer);
 
/*
 * Bildet eine Checksumme aus Inhalt in uebergebenem Pointer
 * @param f_buffer Pointer auf Inhalt
 * @return Checksumme im Format uint32_t
 */
extern "C" uint32_t checksum(uint16_t* _buffer, uint8_t _number_int16_t);

//Kopiert den Lese-Buffer in einem zu bearbeitenden Datenfeld und bildet die Checksumme
extern "C" uint32_t cp_checksum(uint16_t* _buffer, uint8_t _number_int16_t, uint16_t* daten_struktur); //tDatenInvHand daten_struktur -> (uint16_t*)&variable_daten_struktur Nutzung:daten_struktur[i]

//Kopiert den Lese-Buffer in einem zu bearbeitenden Datenfeld 
extern "C" void cp_daten(uint16_t* _buffer, uint8_t _number_int16_t, uint16_t* daten_struktur);




#endif
