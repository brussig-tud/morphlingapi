
#ifndef __THREAD_H__
#define __THREAD_H__


//////
//
// Namespaces
//

// Implemented namespace
namespace Morphling {



//////
//
// Functions
//

/**
 * @brief
 *		Converts a given pointer to a @ref String, to help with creating unique names
 *		for the various threading objects.
 *
 * @param pointer
 *		The pointer to name-ify.
 */
String nameFromPointer(void* pointer);



//////
//
// Classes
//

/**
 * @brief
 *		Simple thread class necessary because of Visual Studio's utterly stupid "no
 *		std::thread in CLR mode" rule.
 */
class Thread
{

protected:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


public:

	////
	// Object construction / destruction

	/** @brief Default constructor. */
	Thread();

	/** @brief The destructor. */
	virtual ~Thread() noexcept(false);


	////
	// Methods

	/** @brief Starts thread execution. */
	void start (void);

	/** @brief Checks if @ref #start was already dispatched. */
	bool isStarted (void);

	/**
	 * @brief
	 *		Notify the thread that it should gracefully exit. Threads must check @ref
	 *		#shutdownRequested to be able to react to this.
	 */
	void shutdown (void);

	/**
	 * @brief
	 *		Checks if graceful exit of the thread has been requested via @ref
	 *		#shutdown .
	 */
	bool shutdownRequested(void);

	/**
	 * @brief
	 *		Waits for the thread to exit, optionally with a timeout.
	 *
	 * @return
	 *		@c true if the thread terminated, @c false if it did not terminate within the
	 *		specified timeout (if any).
	 *
	 * @param timeout
	 *		The amount of time, in milliseconds, to wait for. The default of 0 indicates
	 *		no timeout - meaning the function might block forever if the thread is
	 *		unresponsive.
	 */
	bool wait (unsigned timeout=0);

	/** @brief Thread execution entry point, returning the thread exit code. */
	virtual unsigned run (void) = 0;

	/**
	 * @brief Makes the calling thread block for a given amount of milliseconds.
	 *
	 * @param milliseconds
	 *		Time, in milliseconds, to block for.
	 */
	static void sleep (unsigned milliseconds);

	/** @brief Makes the calling thread yields the CPU / CPU core to other threads. */
	static void yield (void);
};

/**
 * @brief
 *		Simple mutex class necessary because of Visual Studio's utterly stupid "no
 *		std::thread in CLR mode" rule.
 */
class Mutex
{

protected:

	////
	// Data members

	/** @brief Used internally. */
	void *__handle;


public:

	////
	// Object construction / destruction

	/** @brief Default constructor. */
	Mutex(const String &name=L"");

	/** @brief The destructor. */
	~Mutex();


	////
	// Methods

	/** @brief Acquire the mutex for the current thread. */
	void acquire (void);

	/** @brief Release the mutex from the current thread. */
	void release (void);
};

/**
 * @brief
 *		Simple lock class necessary because of Visual Studio's utterly stupid "no
 *		std::thread in CLR mode" rule.
 */
class Lock
{

protected:

	////
	// Data members

	/** @brief The mutex to lock. */
	Mutex *mutex;


public:

	////
	// Object construction / destruction

	/**
	 * @brief Constructs the lock for the given mutex.
	 *
	 * @param mutex
	 *		The @link MorphlingAPI::Mutex mutex object @endlink to protect with this
	 *		lock.
	 */
	Lock(Mutex &mutex);

	/** @brief The destructor. */
	~Lock();


	////
	// Methods

	/** @brief Explicitly unlock before the lock goes out of scope. */
	void unlock (void);
};

/** @brief A simple signal-able event for inter-thread synchronization. */
class Event
{

protected:

	////
	// Data members

	/** @brief Used internally. */
	void *__handle;


public:

	////
	// Object construction / destruction

	/** @brief Default constructor. */
	Event(const String &name = L"");

	/** @brief The destructor. */
	~Event();


	////
	// Methods

	/** @brief Signal the event. */
	void signal (void);

	/**
	 * @brief Wait for the event to be signaled.
	 *
	 * @return
	 *		@c true if the event was actually signaled, @c false if the timeout, if any,
	 *		expired. Note that not specifying a timeout means that the method will always
	 *		return @c true, if it returns.
	 *
	 * @param timeout
	 *		The timeout, in milliseconds, until the wait is cancelled. The event can
	 *		still be waited for afterwards. Passing @c 0 disables the timeout.
	 */
	bool wait (unsigned timeout);

	/** @brief Resets the event to non-signaled. */
	void reset (void);
};

/** @brief A periodic timer class to drive realtime system control loops. */
class Heartbeat
{

protected:

	////
	// Data members

	/** @brief Implementation handle. */
	void *pimpl;


public:

	////
	// Object construction / destruction

	/**
	* @brief Constructs the timer with a given period.
	*
	* @param period
	*		The period, in microseconds, of the heartbeat. The construction will fail if
	*		the given period can not be adequately approximated by the platform.
	*/
	Heartbeat(unsigned period);

	/** @brief The destructor. */
	~Heartbeat();


	////
	// Methods

	/** @brief Waits until the next beat begins. */
	void wait (void);

	/** @brief Returns the time, in milliseconds, since the heartbeat started. */
	double elapsedTime (void);

	/** @brief Returns the number of beats since the beat began. */
	unsigned long long elapsedBeats (void);

	/** @brief Returns the number of misses since the beat began. */
	unsigned skippedBeats (void);

	/**
	 * @brief
	 *		Returns the timestamp of the current beat, in milliseconds, since the start
	 *		of the heartbeat.
	 */
	double getCurrentBeatTimestamp (void);

	/**
	 * @brief
	 *		Returns the last deviation, in milliseconds, from the next scheduled beat
	 *		time point when the timer woke up from the blocked state.
	 *
	 * A negative deviation means the thread woke up in time. Smaller absolute values
	 * indicate better timer efficiency - the closer the deviation is to -0, the less
	 * time had to be spend spinning the CPU.
	 */
	double lastWakeDeviation (void);

	/**
	 * @brief Sets the beat timer to have the specified period.
	 *
	 * @param period
	 *		The period, in microseconds, of the heartbeat. The method will fail if the
	 *		given period can not be adequately approximated by the platform, leaving the
	 *		timer in an undefined state.
	 */
	void changePeriod (unsigned period);

	/**
	 * @brief
	 *		Restarts the heartbeat, so that @ref #elapsedTime will return the duration
	 *		since this method was called.
	 *
	 * @param period
	 *		Optionally, sets the period, in microseconds, of the heartbeat after the
	 *		restart. The method will fail if the given period can not be adequately
	 *		approximated by the platform, leaving the timer in an undefined state.
	 */
	void reset (unsigned period);

	/** @brief . */
	static unsigned getMinPossibleTimerPeriod (void);

	/** @brief . */
	static unsigned getClosestNativeTimerPeriod (unsigned period);

	/** @brief . */
	static unsigned getProcessWideTimerPeriod (void);

	/** @brief . */
	static unsigned setProcessWideTimerPeriod (unsigned period);
};


// Close implemented namespace
}


#endif // ifndef __THREAD_H__
