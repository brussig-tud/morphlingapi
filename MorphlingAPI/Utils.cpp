
//////
//
// Includes
//

// C++ STL and Boost
#if (_MSC_VER < 1910)
	#include <boost/any.hpp>
#else
	#include <any>
#endif
#include <vector>
#include <string>
#include <sstream>

// .NET CLR utilities
#include <vcclr.h>

// Windows API
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

// Implemented header
#include "Utils.h"



//////
//
// Namespaces
//

// Implemented namespace
using namespace Morphling;



//////
//
// Module initialization and globals
//

// [BEGIN] Module-private global namespace
namespace {

// Failure string for getSystemErrorMsg() with platform-specific line break determined at
// runtime, during dll load
String unknownErrorCodeMsg;

// Takes care of intialization on dll load
struct Initializer
{
	Initializer()
	{
		std::basic_stringstream<TCHAR> str;
		str << L"  <UNABLE TO OBTAIN ERROR DESCRIPTION>" << std::endl;
		unknownErrorCodeMsg = std::move(str.str());
	}
} initializer;

// Size of on-stack error string buffers
constexpr const unsigned errorDescBufLen = 4096;

// [END] Module-private global namespace
}



//////
//
// Local functions
//

void streamVarValuePairs (std::basic_ostringstream<TCHAR> &stream,
                          const VarValueList &pairs)
{
	for (auto it : pairs)
	{
		stream << it.first << L" = ";
		// Try to print the parameter value
		{ if (String *str = anyCast<String>(it.second))
			stream << L'"' << *str << L'"';
		else if (const TCHAR **str = anyCast<const TCHAR*>(it.second))
			stream << L'"' << *str << L'"';
		else if (TCHAR **str = anyCast<TCHAR*>(it.second))
			stream << L'"' << *str << L'"';
		else if (unsigned *n = anyCast<unsigned>(it.second))
			stream << *n;
		else if (signed *n = anyCast<signed>(it.second))
			stream << *n;
		else if (unsigned long *n = anyCast<unsigned long>(it.second))
			stream << *n;
		else if (long *n = anyCast<long>(it.second))
			stream << *n;
		else if (unsigned short *n = anyCast<unsigned short>(it.second))
			stream << *n;
		else if (short *n = anyCast<short>(it.second))
			stream << *n;
		else if (unsigned long long *n = anyCast<unsigned long long>(it.second))
			stream << *n;
		else if (long long *n = anyCast<long long>(it.second))
			stream << *n;
		else if (float *n = anyCast<float>(it.second))
			stream << *n;
		else if (double *n = anyCast<double>(it.second))
			stream << *n;
		else if (const void **ptr = anyCast<const void*>(it.second))
			stream << "0x" << std::hex << *ptr;
		else if (void **ptr = anyCast<void*>(it.second))
			stream << "0x" << std::hex << *ptr;
		else
			stream << L"<UNKNOWN>"; }
		stream << std::endl;
	}
}



//////
//
// Function implementations
//

// getSystemErrorMsg
String Morphling::getSystemErrorMsg (unsigned errorCode)
{
	TCHAR errorDesc [errorDescBufLen];
	if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nullptr, errorCode, 0, errorDesc,
	                  errorDescBufLen, nullptr))
		return errorDesc;
	else
		return unknownErrorCodeMsg;
}

// formatExceptionMsg
String Morphling::formatExceptionMsg (
	const String &moduleDesc, const String &errorDesc, const VarValueList &params
)
{
	std::basic_ostringstream<TCHAR> msg;
	msg << L"[" << moduleDesc << L"] " << errorDesc;
	if (!params.empty()) msg << std::endl;
	streamVarValuePairs(msg, params);
	return std::move(msg.str());
}

String Morphling::formatExceptionMsg (
	unsigned errorCode, const String &moduleDesc, const String &errorDesc,
	const VarValueList &params
)
{
	std::basic_ostringstream<TCHAR> msg;
	msg << L"[" << moduleDesc << L"] " << errorDesc << std::endl;
	streamVarValuePairs(msg, params);
	msg << L"Reason:" << std::endl << "  Error code " << errorCode << std::endl
	    << L"  " << std::move(getSystemErrorMsg(errorCode));
	return std::move(msg.str());
}

// formatManagedExceptionMsg
System::String^ Morphling::formatManagedExceptionMsg (
	const String &moduleDesc, const String &errorDesc, const VarValueList &params
)
{
	return gcnew System::String(
		std::move(formatExceptionMsg(moduleDesc, errorDesc, params)).c_str()
	);
}

System::String^ Morphling::formatManagedExceptionMsg (
	unsigned errorCode, const String &moduleDesc, const String &errorDesc,
	const VarValueList &params
)
{
	return gcnew System::String(
		std::move(formatExceptionMsg(errorCode, moduleDesc, errorDesc, params)).c_str()
	);
}

System::String^ Morphling::formatManagedExceptionMsg (
	System::Exception^ inner, const String &moduleDesc, const String &errorDesc,
	const VarValueList &params
)
{
	// Pin down inner exception desc
	const cli::pin_ptr<const TCHAR> inner_msg = PtrToStringChars(inner->ToString());

	// Compile error message
	std::basic_ostringstream<TCHAR> msg;
	msg << std::move(formatExceptionMsg(moduleDesc, errorDesc, params)) << std::endl
	    << "Reason:" << std::endl << L"  " << inner_msg;

	// Done!
	return gcnew System::String(msg.str().c_str());
}
