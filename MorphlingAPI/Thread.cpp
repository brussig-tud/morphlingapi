﻿
//////
//
// Includes
//

// C++ STL and Boost
#if (_MSC_VER < 1910)
	#include <boost/any.hpp>
#else
	#include <any>
#endif
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <chrono>
#include <exception>
#include <stdexcept>
#include <algorithm>

// Windows API
#define NOMINMAX
#include <Windows.h>

// Local includes
#include "Utils.h"

// Implemented header
#include "Thread.h"



//////
//
// Namespaces
//

// Implemented namespace
using namespace Morphling;



//////
//
// Module-private globals
//

// No "storage class specifier 'register' is deprecated" warnings
#pragma warning (disable : 5033)

// [BEGIN] Module-private global namespace
namespace {

// Delegates Win32 thread execution to a MorphlingAPI::Thread object
static DWORD WINAPI threadMain (LPVOID param)
{
	return ((Thread*)param)->run();
}

// [END] Module-private global namespace
}



//////
//
// Function implementation
//

String Morphling::nameFromPointer(void *pointer)
{
	std::basic_ostringstream<TCHAR> sstr;
	sstr << std::hex << (size_t)pointer;
	return std::move(sstr.str());
}



//////
//
// Class implementation
//

////
// Thread

struct Thread_impl
{
	bool isStarted, msgShutdown;
	HANDLE threadHandle;
	Mutex startMutex;

	Thread_impl(Thread *parent)
		: isStarted(false), msgShutdown(false), threadHandle(nullptr),
		  startMutex(L"Thread_impl_startMutex__"+nameFromPointer(this))
	{
		threadHandle = CreateThread(
			nullptr, 0, threadMain, parent, CREATE_SUSPENDED, nullptr
		);
		if (!threadHandle)
			throw gcnew System::Exception(formatManagedExceptionMsg(
				GetLastError(), L"MorphlingAPI::Thread", L"Unable to create thread"
				L" object."
			));
	}
	~Thread_impl() noexcept(false)
	{
		if (threadHandle)
		{
			if (isStarted)
			{
				DWORD reason = WaitForSingleObject(threadHandle, 2048);
				if (reason == WAIT_TIMEOUT || reason == WAIT_FAILED)
					TerminateThread(threadHandle, -1);
				CloseHandle(threadHandle);
				HANDLE __handle_bak = threadHandle;
				threadHandle = nullptr;
				if (reason == WAIT_TIMEOUT)
					throw gcnew System::TimeoutException(formatManagedExceptionMsg(
						reason, L"MorphlingAPI::Thread",
						L"Unable to gracefully shutdown thread.",
						{{ L"threadHandle", __handle_bak }}
				));
				if (reason == WAIT_FAILED)
					throw gcnew System::Exception(formatManagedExceptionMsg(
						GetLastError(),  L"MorphlingAPI::Thread",
						L"Unable to gracefully shutdown worker thread.",
						{{ L"threadHandle", __handle_bak }}
					));
			}
			else
			{
				TerminateThread(threadHandle, 1);
				CloseHandle(threadHandle);
				threadHandle = nullptr;
			}
		}
	}
};
#define THREAD_IMPL ((Thread_impl*)pimpl)

Thread::Thread() : pimpl(nullptr)
{
	// Allocate implementation instance
	pimpl = new Thread_impl(this);
	if (pimpl == nullptr)
		throw gcnew System::OutOfMemoryException(L"[MorphlingAPI::Thread] Failed to allocate class instance.");
}

Thread::~Thread() noexcept(false)
{
	if (pimpl)
	{
		delete THREAD_IMPL;
		pimpl = nullptr;
	}
}

void Thread::start (void)
{
	// Shortcut to save one indirection per member access
	register Thread_impl *impl = THREAD_IMPL;

	// Lock start state mutex
	Lock lock(impl->startMutex);

	// Try to wake thread from initial suspension
	signed result;
	do { result = ResumeThread(impl->threadHandle); }
	while (result > 1);

	// Deal with the aftermath
	if (result == -1)
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to start thread.",
			{{ L"threadHandle", impl->threadHandle }}
		));

	// Successfully started!
	impl->isStarted = true;
}

bool Thread::isStarted (void)
{
	// Shortcut to save one indirection per member access
	register Thread_impl *impl = THREAD_IMPL;

	// Lock start state mutex
	Lock lock(impl->startMutex);

	// Report state
	return impl->isStarted;
}

void Thread::shutdown(void)
{
	THREAD_IMPL->msgShutdown = true;
}

bool Thread::shutdownRequested(void)
{
	return THREAD_IMPL->msgShutdown;
}

bool Thread::wait (unsigned timeout)
{
	// Effective timeout
	DWORD t = timeout > 0 ? timeout : 1024;

	// Wait loop
	DWORD reason = WAIT_TIMEOUT;
	do {
		reason = WaitForSingleObject(THREAD_IMPL->threadHandle, t);
		if (reason == WAIT_FAILED)
			throw gcnew System::Exception(formatManagedExceptionMsg(
				GetLastError(), L"MorphlingAPI::Thread",
				L"Unable to wait for thread.",
				{ { L"threadHandle", THREAD_IMPL->threadHandle } }
		));
		if (reason == WAIT_TIMEOUT && timeout > 0)
			// The optional timeout was hit
			return false;
	}
	while (reason == WAIT_TIMEOUT);

	// Indicate success
	return true;
}

void Thread::sleep (unsigned milliseconds)
{
	Sleep(milliseconds);
}

void Thread::yield (void)
{
	SwitchToThread();
}


////
// Mutex

Mutex::Mutex(const String &name) : __handle(nullptr)
{
	if (name.empty())
		__handle = CreateMutex(nullptr, false, nullptr);
	else
		__handle = CreateMutex(nullptr, false, (L"Local\\"+name).c_str());
	if (!__handle)
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to create mutex object."
		));
}

Mutex::~Mutex()
{
	if (__handle)
	{
		WaitForSingleObject(__handle, 2048);
		CloseHandle(__handle);
		__handle = nullptr;
	}
}

void Mutex::acquire (void)
{
	DWORD reason = WAIT_TIMEOUT;
	do reason = WaitForSingleObject(__handle, 10240);
	while (reason == WAIT_TIMEOUT);

	if (reason == WAIT_FAILED)
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(),  L"MorphlingAPI::Thread",
			L"Unable to request ownership of mutex.",
			{{ L"__handle", __handle }}
		));
}

void Mutex::release (void)
{
	if (!ReleaseMutex(__handle))
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(),  L"MorphlingAPI::Thread",
			L"Unable to release ownership of mutex.",
			{{ L"__handle", __handle }}
		));
}


////
// Lock

Lock::Lock(Mutex &mutex) : mutex(&mutex)
{
	mutex.acquire();
}

Lock::~Lock()
{
	if (mutex)
	{
		mutex->release();
		mutex = nullptr;
	}
}

void Lock::unlock (void)
{
	mutex->release();
	mutex = nullptr;
}


////
// Event

Event::Event(const String &name) : __handle(nullptr)
{
	if (name.empty())
		__handle = CreateEvent(nullptr, true, false, nullptr);
	else
		__handle = CreateEvent(nullptr, true, false, (L"Local\\" + name).c_str());
	if (!__handle)
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to create event object."
		));
}

Event::~Event()
{
	if (__handle)
	{
		CloseHandle(__handle);
		__handle = nullptr;
	}
}

void Event::signal (void)
{
	if (!SetEvent(__handle))
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to signal event object.",
			{{ L"__handle", __handle }}
		));
}

bool Event::wait (unsigned timeout)
{
	DWORD reason = WaitForSingleObject(__handle, timeout==0 ? INFINITE : timeout);
	if (reason == WAIT_FAILED)
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to wait for event signal.",
			{{ L"__handle", __handle }}
		));
	return reason == 0;
}

void Event::reset (void)
{
	if (!ResetEvent(__handle))
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to reset event object.",
			{{ L"__handle", __handle }}
	));
}


////
// Heartbeat

// Undocumented NTDLL APIs used in this class
extern "C" NTSYSAPI NTSTATUS NTAPI NtQueryTimerResolution (
	OUT PULONG MinimumResolution,
	OUT PULONG MaximumResolution,
	OUT PULONG CurrentResolution
);
extern "C" NTSYSAPI NTSTATUS NTAPI NtSetTimerResolution (
	IN ULONG   DesiredResolution,
	IN BOOLEAN SetResolution,
	OUT PULONG CurrentResolution
);

// STL clock types
typedef std::chrono::high_resolution_clock::time_point SysTimePoint;
typedef std::chrono::nanoseconds SysCycleDuration;
typedef std::chrono::nanoseconds::rep SysCycleCount;

// Hidden implementation
struct Heartbeat_impl
{
	HANDLE timerHandle;

	SysTimePoint beginning, firstBeat, currentBeat;
	SysCycleDuration slice_blk, slice_full, lastDeviation;
	unsigned long long beatCounter, calib;
	unsigned skippedBeats;

	ULONG defaultTimerRes;

	Heartbeat_impl() : timerHandle(nullptr), beatCounter(0), skippedBeats(0)
	{}

	~Heartbeat_impl()
	{
		if (timerHandle)
		{
			CancelWaitableTimer(timerHandle);
			CloseHandle(timerHandle);
			timerHandle = nullptr;
		}
	}
};
#define HEARTBEAT_IMPL ((Heartbeat_impl*)pimpl)

Heartbeat::Heartbeat(unsigned period) : pimpl(nullptr)
{
	// Allocate implementation instance
	pimpl = new Heartbeat_impl;
	if (pimpl == nullptr)
		throw gcnew System::OutOfMemoryException(L"[MorphlingAPI::Thread] Failed to"
		                                         L" allocate class instance.");
	register Heartbeat_impl* impl = HEARTBEAT_IMPL;

	// Setup timer object
	impl->timerHandle = CreateWaitableTimer(
		nullptr, true, (L"Local\\Heartbeat__"+nameFromPointer(this)).c_str()
	);
	if (!(impl->timerHandle))
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to create timer object."
		));

	// Check feasability of period
	ULONG max, sysPeriod; { ULONG min; NtQueryTimerResolution(&min, &max, &sysPeriod); }
	impl->defaultTimerRes = sysPeriod;
	NtSetTimerResolution(max, true, &sysPeriod);
	unsigned period_100ns = period*10;
	if (period_100ns < sysPeriod)
		throw gcnew System::NotSupportedException(formatManagedExceptionMsg(
			L"MorphlingAPI::Thread", L"The platform cannot provide the desired timer"
			L" period.",
			{{ L"period", period },
			 { L"<lowest possible>", sysPeriod/10.0 }}
		));

	// Apply some heuristics to get a good mix of blocking and CPU spinning
	unsigned truncated = (period_100ns/sysPeriod) * sysPeriod,
	         empiricalMulti = period<1250 ? 2 : 3;
	impl->slice_blk = std::move(SysCycleDuration(
		std::max<unsigned>(truncated, sysPeriod*empiricalMulti)*100
	));
	impl->slice_full = std::move(SysCycleDuration(period_100ns*100));
	impl->lastDeviation = SysCycleDuration::zero();

	// Calibrate
	SYSTEMTIME syst; Sleep(2);
	GetSystemTime(&syst); impl->beginning = std::chrono::high_resolution_clock::now();
	ULARGE_INTEGER sys100ns; SystemTimeToFileTime(&syst, (FILETIME*)&sys100ns);
	impl->calib = sys100ns.QuadPart - (impl->beginning.time_since_epoch().count()/100);
	impl->currentBeat = impl->firstBeat = impl->beginning;
	SysTimePoint first = impl->firstBeat + impl->slice_blk;

	// Set timer (all durations in multiples of 100ns)
	LARGE_INTEGER firstSignal;
	firstSignal.QuadPart = first.time_since_epoch().count()/100 + impl->calib;
	if (!SetWaitableTimer(impl->timerHandle, &firstSignal, 0, nullptr, nullptr, false))
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to set up timer object."
		));
}

Heartbeat::~Heartbeat()
{
	if (pimpl)
	{
		ULONG sysPeriod; NtSetTimerResolution(
			HEARTBEAT_IMPL->defaultTimerRes, true, &sysPeriod
		);
		delete HEARTBEAT_IMPL;
		pimpl = nullptr;
	}
}

void Heartbeat::wait (void)
{
	// Shortcut to save one indirection per member access
	register Heartbeat_impl *impl = HEARTBEAT_IMPL;

	// PREcaution
	ULONG max, sysPeriod; { ULONG min; NtQueryTimerResolution(&min, &max, &sysPeriod); }
	NtSetTimerResolution(max, true, &sysPeriod);

	// Wait for timer event
	DWORD reason = WaitForSingleObject(impl->timerHandle, INFINITE);
	if (reason == WAIT_FAILED)
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to wait for periodic"
			L" timer event.", {{ L"timerHandle", impl->timerHandle }}
		));
	if (reason == WAIT_TIMEOUT)
		throw gcnew System::TimeoutException(formatManagedExceptionMsg(
			reason, L"MorphlingAPI::Thread", L"Unable to wait for periodic timer event.",
			{{ L"timerHandle", impl->timerHandle }}
		));

	// Get an idea when exactly we woke up
	impl->currentBeat = std::chrono::high_resolution_clock::now();
	SysTimePoint nextBeat = impl->firstBeat + impl->slice_full*(impl->beatCounter+1);
	impl->lastDeviation = impl->currentBeat - nextBeat;
	if (impl->lastDeviation > SysCycleDuration::zero())
	{
		// We missed the next beat, possibly by several slices.
		unsigned skipped = unsigned(impl->lastDeviation/impl->slice_full) + 1;
		impl->skippedBeats += skipped;
		impl->beatCounter += skipped;
		nextBeat = impl->firstBeat + impl->slice_full*(impl->beatCounter);
	}
	else
		// We woke up in time. Increment beat counter.
		impl->beatCounter++;

	// Try to save some more CPU time if there was significent undershoot
	/*if (impl->lastDeviation.count() < -signed(sysPeriod*200))
	{
		/*signed count = std::max(
			signed(std::abs(impl->lastDeviation.count()))/signed(sysPeriod*100) - 1, 1
		);*//*
		LARGE_INTEGER relTime; relTime.QuadPart = //-signed(count*sysPeriod)
			nextBeat.time_since_epoch().count()/100 + impl->calib;
		SetWaitableTimer(impl->timerHandle, &relTime, 0, nullptr, nullptr, false);
		WaitForSingleObject(impl->timerHandle, INFINITE);
		impl->currentBeat = std::chrono::high_resolution_clock::now();
		impl->lastDeviation = impl->currentBeat - nextBeat;
	}*/

	// Spin the CPU until the precise start of the next beat
	while (impl->currentBeat < nextBeat)
		impl->currentBeat = std::chrono::high_resolution_clock::now();

	// POSTcaution
	{ ULONG min; NtQueryTimerResolution(&min, &max, &sysPeriod); }
	NtSetTimerResolution(max, true, &sysPeriod);

	// Set up next signal
	LARGE_INTEGER nextSignal; nextSignal.QuadPart =
		(nextBeat + impl->slice_blk).time_since_epoch().count()/100 + impl->calib;
	if (!SetWaitableTimer(impl->timerHandle, &nextSignal, 0, nullptr, nullptr, false))
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to set up timer object."
		));
}

double Heartbeat::elapsedTime (void)
{
	SysCycleDuration sinceBeginning =
		std::chrono::high_resolution_clock::now() - HEARTBEAT_IMPL->beginning;
	return double(sinceBeginning.count()) / 1000000.0;
}

unsigned long long Heartbeat::elapsedBeats (void)
{
	return HEARTBEAT_IMPL->beatCounter;
}

unsigned Heartbeat::skippedBeats (void)
{
	return HEARTBEAT_IMPL->skippedBeats;
}

double Heartbeat::getCurrentBeatTimestamp (void)
{
	SysCycleDuration timestamp =
		HEARTBEAT_IMPL->currentBeat - HEARTBEAT_IMPL->beginning;

	return double(timestamp.count()) / 1000000.0;
}

double Heartbeat::lastWakeDeviation (void)
{
	return double(HEARTBEAT_IMPL->lastDeviation.count()) / 1000000.0;
}

void Heartbeat::changePeriod (unsigned period)
{
	// Shortcut to save one indirection per member access
	register Heartbeat_impl *impl = HEARTBEAT_IMPL;

	// Check if we even need to do anything
	if (period == impl->slice_full.count()/1000) return;

	// Check feasability of new period
	ULONG max, sysPeriod; { ULONG min; NtQueryTimerResolution(&min, &max, &sysPeriod); }
	NtSetTimerResolution(max, true, &sysPeriod);
	unsigned period_100ns = period*10;
	if (period_100ns < sysPeriod)
		throw gcnew System::NotSupportedException(formatManagedExceptionMsg(
			L"MorphlingAPI::Thread", L"The platform cannot provide the desired timer"
			L" period.",
			{{ L"period", period },
			{ L"<lowest possible>", sysPeriod/10.0 }}
		));

	// Clean up old timer
	CancelWaitableTimer(impl->timerHandle);

	// Apply some heuristics to get a good mix of blocking and CPU spinning
	unsigned truncated = period_100ns - (period_100ns%sysPeriod),
	         empiricalMulti = period<1250 ? 2 : 3;
	impl->slice_blk = std::move(SysCycleDuration(
		std::max<unsigned>(truncated, sysPeriod*empiricalMulti)*100
	));
	impl->slice_full = std::move(SysCycleDuration(period_100ns*100));

	// Set new timer
	// - align tick
	Sleep(2); impl->firstBeat = std::chrono::high_resolution_clock::now();
	// - determine timer parameters
	impl->currentBeat = impl->firstBeat;
	SysTimePoint next = impl->currentBeat + impl->slice_blk;
	impl->beatCounter = 0;
	// - setup OS timer
	LARGE_INTEGER nextSignal;
	nextSignal.QuadPart = next.time_since_epoch().count()/100 + impl->calib;
	if (!SetWaitableTimer(impl->timerHandle, &nextSignal, 0, nullptr, nullptr, false))
		throw gcnew System::Exception(formatManagedExceptionMsg(
			GetLastError(), L"MorphlingAPI::Thread", L"Unable to set up timer object."
		));
}

void Heartbeat::reset (unsigned period)
{
	// Tick changePeriod() into doing the actual reset work
	HEARTBEAT_IMPL->slice_full = SysCycleDuration::zero();
	changePeriod(period);

	// Reset beginning timestamp
	register Heartbeat_impl *impl = HEARTBEAT_IMPL;
	impl->beginning = impl->firstBeat;
}

unsigned Heartbeat::getMinPossibleTimerPeriod (void)
{
	ULONG maxPeriod, minPeriod, curPeriod;
	NtQueryTimerResolution(&maxPeriod, &minPeriod, &curPeriod);
	return minPeriod / 10; // Convert from 100ns unit to 1μs unit
}

unsigned Heartbeat::getClosestNativeTimerPeriod (unsigned period)
{
	ULONG max, min, cur;
	NtQueryTimerResolution(&max, &min, &cur);

	ULONG
		desiredPeriod = period*10,
		remainder = desiredPeriod % cur,
		truncated = (desiredPeriod/cur) * cur;

	if (remainder == 0)
		return period;
	else if (remainder > cur/2)
		return (truncated+cur) / 10; // ─┐
	else                             //  ├ Convert from 100ns unit to 1μs unit
		return truncated / 10;       // ─┘
}

unsigned Heartbeat::getProcessWideTimerPeriod (void)
{
	ULONG max, min, cur;
	NtQueryTimerResolution(&max, &min, &cur);
	return cur / 10; // Convert from 100ns unit to 1μs unit
}

unsigned Heartbeat::setProcessWideTimerPeriod (unsigned period)
{
	ULONG dummy, cur = getProcessWideTimerPeriod();
	NtSetTimerResolution(period*10, true, &dummy);
	return cur;
}
