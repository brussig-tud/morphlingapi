#pragma once


//////
//
// Namespaces
//

// Implemented namespace
/// <summary>The namespace of the Morphling API.</summary>
namespace Morphling {



//////
//
// Classes
//

/// <summary>Central class wrapping the functionality of the morphling control service.
/// </summary>
public ref class Server
{

public:

	////
	// Exported types

	/// <summary>Argument container for the <see cref="MorphlingPressed" /> event. Does
	/// not actually contain any data in this version of the API.</summary>
	ref class MorphlingPressedEventArgs : public System::EventArgs  
	{

	public:

		////
		// Object construction / destruction

		/// <summary>The default constructor.</summary>
		MorphlingPressedEventArgs();

		/// <summary>The destructor.</summary>
		~MorphlingPressedEventArgs();

		/// <summary>The .NET finalizer.</summary>
		!MorphlingPressedEventArgs();
	};


protected:

	////
	// Data members

	/// <summary>Implementation handle.</summary>
	void* pimpl;


public:

	////
	// Object construction / destruction

	/// <summary>Initializes for the morphling control service listening at the given
	/// address on the given port.</summary>
	///
	/// <param name='address'>The server address hosting the morphling service, given
	/// either by name (which will be resolved using system DNS resolution) or by IPv4
	/// address.</param>
	/// <param name='port'>The UDP port on which the morphling service is reachable on
	/// the server.</param>
	Server(System::String^ address, unsigned port);

	/// <summary>The destructor.</summary>
	~Server();

	/// <summary>The .NET finalizer.</summary>
	!Server();


	////
	// Events

	/// <summary>Gets raised when the user presses down all actuators of the morphling.
	/// </summary>
	event System::EventHandler<MorphlingPressedEventArgs^>^ MorphlingPressed;


	////
	// Methods

	/// <summary>Queries the position of the foot point of the given actuator on the
	/// morphling xz plane.</summary>
	///
	/// <returns>A 3-element array representing the 3D position vector of the actuator
	/// foot point on the morphling reference plane. Since the foot point lies on that
	/// plane by definition, the second element (y coordinate) of this vector will always
	/// be zero.</returns>
	///
	/// <param name='actuator'>ID of the actuator.</param>
	array<float>^ getFootpoint (unsigned actuator);

	/// <summary>Queries the positions of all actuator foot points on the morphling xz
	/// plane.</summary>
	///
	/// <returns>An array of 3-element arrays representing the 3D position vectors of all
	/// actuator foot points on the morphling reference plane. Since the foot point lies
	/// on that plane by definition, the second element (y coordinate) of this vector
	/// will always be zero.</returns>
	array<array<float>^ >^ getFootpoints (void);

	/// <summary>Set target height for given actuator.</summary>
	///
	/// <param name='actuator'>ID of the actuator to set the target height for.</param>
	/// <param name='height'>The height, in mm, that the actuator should position itself
	/// at.</param>
	void setHeight (unsigned actuator, float height);

	/// <summary>Set target height for the given actuators. This is more efficient than
	/// individual calls to <see cref="setHeight" /> due to consolidated synchronization
	/// overhead.</summary>
	///
	/// <param name='IDs'>IDs of the actuators that should get their target heights
	/// set.</param>
	/// <param name='heights'>The target heights, in mm, that the actuators should
	/// position themselves at.</param>
	void setHeights (array<int>^ IDs, array<float>^ heights);

	/// <summary>Set target height for all actuators. This is more efficient than
	/// individual calls to <see cref="setHeight" /> or <see cref="setHeights" /> with
	/// subsets of actuators due to consolidated synchronization overhead.</summary>
	///
	/// <param name='pose'>The target heights for all actuators, in mm, that they should
	/// position themselves at.</param>
	void setPose (array<float>^ pose);

	/// <summary>Retreives the current position of all actuators.</summary>
	///
	/// <returns>An array containing the height, in millimeters, of each
	/// actuator.</returns>
	array<float>^ getPose (void);

	/// <summary>Sets the actuator height that should signify a "pressed down" state. The
	/// default is <c>0</c>, which basically disables the detection of the interation.
	/// </summary>
	///
	/// <param name='height'>The actuator height, in millimeters, below which the
	/// morphling is considered to be "pressed down". Set this to <c>0</c> or a negative
	/// value to effectively disable detection.</param>
	void setMorphlingPressedThreshold (float height);

	/// <summary>Set peak current for actuators.</summary>
	///
	/// <param name='value'>The current, in amperes, that the actuators are allowed to
	/// peak to when moving.</param>
	void setPeakCurrent (float value);

	/// <summary>The current, in amperes, the actuators should maintain on
	/// average.</summary>
	///
	/// <param name='value'>The current, in amperes, that the actuators should maintain
	/// on average.</param>
	void setMeanCurrent (float value);

	/// <summary>An shortcut for calling both <see cref="setPeakCurrent"/> with a value
	/// of 0.06 amperes and <see cref="setMeanCurrent"/> with a value of 0.350386918
	/// amperes, resulting in the actuators being easy to press down.</summary>
	void switchToSoft (void);

	/// <summary>An shortcut for calling both <see cref="setPeakCurrent"/> with a value
	/// of 1.4 amperes and <see cref="setMeanCurrent"/> with a value of 0.48 amperes,
	/// causing the actuators to be very resistant to being pressed down.</summary>
	void switchToHard (void);

	/// <summary>Set the cycle period of the control thread.</summary>
	///
	/// <param name='period'>The period, in milliseconds, between morphling communication
	/// ticks.</param>
	void setControlPeriod (unsigned period);

	/// <summary>Set the current frame time of your calling application. Enables
	/// interpolation between subsequent actuator position change requests for smoothness
	/// up to the resolution implied by <see cref="setControlPeriod" />. Useful for
	/// applications that run at much less than 200Hz.</summary>
	///
	/// <param name='frametime'>The current frame time, in seconds, of your application.
	/// Setting this to <c>0</c> (the default) or a negative value disables
	/// interpolation.</param>
	void setFrametime (float frametime);

	/// <summary>Initiate a sine wave pattern.</summary>
	void startSineWave (void);

	/// <summary>Stop the sine wave pattern if it is currently running.</summary>
	void stopSineWave (void);

	/// <summary>Sets the parameters of the sine wave sampled by
	/// <see cref="startSineWave" />.</summary>
	///
	/// <param name='periodLength'>The length, in millimeters, of one sine period.</param>
	/// <param name='movementSpeed'>The speed, in millimeters per second, that the
	/// morphling should move over the sine landscape.</param>
	void setSineWaveParams (float periodLength, float movementSpeed);

	/// <summary>Initiates the startup procedure on the morphling. It will take the
	/// morphling about 15 seconds until it can actually process commands.</summary>
	///
	/// <param name='block'>Indicates whether the call should block until the morphling
	/// indicates that it is ready.</param>
	void startup (bool block);

	/// <summary>Initiates the shutdown procedure on the morphling. It will be unable to
	/// process new commands until <see cref="startup" /> is called.</summary>
	void shutdown (void);

	/// <summary>Used internally. Needed as a public method to account for stupid
	/// Microsoft restrictions when trying to seperate interface and implementation with
	/// .NET events. No further documentation, move on, nothing to see here!!!</summary>
	void raiseMPE (void);
};

/// <summary>Tag-along class for controlling robot arm extension / retraction and misc FH
/// main control program interaction (config reload etc.)</summary>
public ref class RobotArm
{

public:

	////
	// Exported types

	/// <summary>Type of state change that the <see cref="StateChanged" /> event signals
	/// - used in conjunction with the <see cref="StateChangedEventArgs" /> container.
	/// </summary>
	enum class StateChange
	{
		/// <summary>Indicates that the robot arm state is now "extended".</summary>
		RA_EXTENDED,

		/// <summary>Indicates that the robot arm state is now "retracted".</summary>
		RA_RETRACTED
	};

	/// <summary>Argument container for the <see cref="StateChanged" /> event.</summary>
	ref class StateChangedEventArgs : public System::EventArgs
	{

	private:

		////
		// Data members

		/// <summary>Value of the property <see cref="newState" />.</summary>
		StateChange newStateValue;

		/// <summary>Value of the property <see cref="success" />.</summary>
		bool successValue;


	public:

		////
		// Object construction / destruction

		/// <summary>Constructs the container with the indicated state change.</summary>
		///
		/// <param name='stateChange'>The state change that the event argument container
		/// should signify. See <see cref="newState" />.</param>
		/// <param name='success'>Indicates if the state change signified by
		/// <c>stateChange</c> was successful or not. See <see cref="success" />.</param>
		StateChangedEventArgs(StateChange stateChange, bool success);

		/// <summary>The destructor.</summary>
		~StateChangedEventArgs();

		/// <summary>The .NET finalizer.</summary>
		!StateChangedEventArgs();


		////
		// Properties

		/// <summary>The new state of the robot arm that this event indicates in case of
		/// state change <see cref="success" />.</summary>
		property StateChange newState {
			public: StateChange get() { return newStateValue; };
		}

		/// <summary>Indicates whether the state change was successful. If <c>false</c>,
		/// then <see cref="newState" /> only indicates the intended target state - the
		/// actual state of the robot arm remains unchanged.</summary>
		property bool success {
			public: bool get() { return successValue; };
		}
	};


protected:

	////
	// Data members

	/// <summary>Implementation handle.</summary>
	void* pimpl;


public:

	////
	// Object construction / destruction

	/// <summary>Initializes for the robot control service listening at the given address
	/// on the given port.</summary>
	///
	/// <param name='address'>The server address hosting the robot control service, given
	/// either by name (which will be resolved using system DNS resolution) or by IPv4
	/// address.</param>
	/// <param name='port'>The TCP port on which the robot control service is
	/// reachable.</param>
	RobotArm(System::String^ address, unsigned port);

	/// <summary>The destructor.</summary>
	~RobotArm();

	/// <summary>The .NET finalizer.</summary>
	!RobotArm();


	////
	// Events

	/// <summary>Gets raised when the robot arm state changed from retracted to extended
	/// and vice-versa.</summary>
	event System::EventHandler<StateChangedEventArgs^>^ StateChanged;


	////
	// Methods

	/// <summary>Tests if the robot control service reacts to commands. This call will
	/// block until a result is obtained.</summary>
	///
	/// <returns><c>true</c> if the robot control service responded with a ping-back,
	/// <c>false</c> if there was no reaction withing 5 seconds.</returns>
	bool ping (void);

	/// <summary>Tells the robot control service to extend the robot arm.</summary>
	///
	/// <param name='block'>Indicates whether the call should block until the robot
	/// control service indicates that the arm is completely extended. Note that the
	/// <see cref="StateChanged" /> event will get triggered either way.</param>
	void extend (bool block);

	/// <summary>Tells the robot control service to retract the robot arm.</summary>
	///
	/// <param name='block'>Indicates whether the call should block until the robot
	/// control service indicates that the arm is completely retracted. Note that the
	/// <see cref="StateChanged" /> event will get triggered either way.</param>
	void retract (bool block);

	/// <summary>Tells the robot control service to reload the main configuration from
	/// disk.</summary>
	void reloadConfig (void);

	/// <summary>Tells the robot control service to start or stop the force-sensor driven
	/// control loop.</summary>
	///
	/// <param name='enabled'>State of the force control loop - <c>true</c> for on and
	/// <c>false</c> for off.</param>
	void enableControl (bool enabled);

	/// <summary>Used internally. Needed as a public method to account for stupid
	/// Microsoft restrictions when trying to seperate interface and implementation with
	/// .NET events. No further documentation, move on, nothing to see here!!!</summary>
	void raiseRAE (StateChangedEventArgs^ eventArgs);
};


// Close implemented namespace
}
