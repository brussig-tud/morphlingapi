
//////
//
// Includes
//

// C++ STL and Boost
#if (_MSC_VER < 1910)          // ToDo: Get rid of the 'any'-include by moving the foot
	#include "Boost/any.hpp"   //       point functionality into the MorphlingComms class
#else
	#include <any>
#endif
#include <vector>
#include <exception>
#include <stdexcept>

// .NET CLR utilities
#include <vcclr.h>

// Windows API
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

// Local includes
#include "Utils.h" // ToDo: Get rid of Utils by moving the foot point stuff as with 'any'
#include "Communication.h"

// Implemented header
#include "MorphlingAPI.h"



//////
//
// Namespaces
//

// Implemented namespace
using namespace Morphling;



//////
//
// Class implementation
//

////
// Server::MorphlingPressedEventArgs

Server::MorphlingPressedEventArgs::MorphlingPressedEventArgs() {}

Server::MorphlingPressedEventArgs::~MorphlingPressedEventArgs() {}

Server::MorphlingPressedEventArgs::!MorphlingPressedEventArgs() {}


////
// Server

struct MorphlingPressedEventTrigger : public MorphlingComms::MorphlingPressedCallback
{
private:
	gcroot<Server^> svr;
public:
	MorphlingPressedEventTrigger(Server^ svr) : svr(svr) {};

	virtual void pressed (void)
	{
		svr->raiseMPE();
	}
};

struct Server_impl
{
	MorphlingComms morphlingComms;
	MorphlingPressedEventTrigger onMorphlingPressed;

	Server_impl(const TCHAR *address, unsigned port, Server ^svr)
		: morphlingComms(address, port), onMorphlingPressed(svr)
	{
		morphlingComms.setMorphlingPressedCallback(onMorphlingPressed);
	}
};
#define SERVER_IMPL ((Server_impl*)pimpl)

Server::Server(System::String^ serverAddress, unsigned port)
	: pimpl(nullptr)
{
	// Convert managed data
	const cli::pin_ptr<const TCHAR> addressString = PtrToStringChars(serverAddress);

	// Allocate implementation instance
	pimpl = new Server_impl(addressString, port, this);
	if (pimpl == nullptr)
		throw gcnew System::OutOfMemoryException(L"[MorphlingAPI::Server] Failed to"
		                                         L" allocate class instance.");
}

Server::~Server()
{
	this->!Server();
}

Server::!Server()
{
	if (pimpl)
	{
		delete SERVER_IMPL;
		pimpl = nullptr;
	}
}

array<float>^ Server::getFootpoint (unsigned actuator)
{
	array<float>^ vec;
	switch (actuator)
	{
	case 0:
		vec = gcnew array<float>(3);
		vec[0] = -65; vec[1] = 0; vec[2] = 0;
		return vec;
	case 1:
		vec = gcnew array<float>(3);
		vec[0] = -35; vec[1] = 0; vec[2] = 0;
		return vec;
	case 2:
		vec = gcnew array<float>(3);
		vec[0] = 0; vec[1] = 0; vec[2] = 0;
		return vec;
	case 3:
		vec = gcnew array<float>(3);
		vec[0] = 35; vec[1] = 0; vec[2] = 0;
		return vec;
	case 4:
		vec = gcnew array<float>(3);
		vec[0] = 65; vec[1] = 0; vec[2] = 0;
		return vec;
	default:
		/* Actuator ID was invalid... throw up right down below */;
	}
	throw gcnew System::ArgumentException(formatManagedExceptionMsg(
		L"MorphlingAPI::Server", L"Specified actuator does not exist.",
		{{ L"actuatorID", actuator }}
	));
}

array<array<float>^ >^ Server::getFootpoints (void)
{
	array<array<float>^ >^ positions = gcnew array<array<float>^ >(5);
	positions[0] = gcnew array<float>(3);
	positions[0][0] = -65; positions[0][1] = 0; positions[0][2] = 0;
	positions[1] = gcnew array<float>(3);
	positions[1][0] = -35; positions[1][1] = 0; positions[1][2] = 0;
	positions[2] = gcnew array<float>(3);
	positions[2][0] = 0; positions[2][1] = 0; positions[2][2] = 0;
	positions[3] = gcnew array<float>(3);
	positions[3][0] = 35; positions[3][1] = 0; positions[3][2] = 0;
	positions[4] = gcnew array<float>(3);
	positions[4][0] = 65; positions[4][1] = 0; positions[4][2] = 0;
	return positions;
}

void Server::setHeight (unsigned actuator, float height)
{
	SERVER_IMPL->morphlingComms.setHeight(actuator, height);
}

void Server::setHeights (array<int>^ IDs, array<float>^ heights)
{
	// Convert to unmanaged arrays
	std::vector<unsigned> _IDs(IDs->Length);
	std::vector<double> _heights(heights->Length);
	for (unsigned i=0; i<_IDs.size(); i++)
	{
		_IDs[i] = IDs[i];
		_heights[i] = heights[i];
	}

	// Send to morphling driver
	SERVER_IMPL->morphlingComms.setHeights(_IDs, _heights);
}

void Server::setPose (array<float>^ pose)
{
	// Convert to unmanaged arrays
	std::vector<double> _pose(pose->Length);
	for (unsigned i=0; i< _pose.size(); i++)
		_pose[i] = pose[i];

	// Send to morphling driver
	SERVER_IMPL->morphlingComms.setPose(_pose);
}

array<float>^ Server::getPose (void)
{
	// Query from morphling driver
	std::vector<double> pose;
	SERVER_IMPL->morphlingComms.getPose(&pose);

	// Convert to managed list
	array<float>^ result = gcnew array<float>((int)pose.size());
	for (unsigned i=0; i< pose.size(); i++)
		result[i] = (float)pose[i];
	return result;
}

void Server::setMorphlingPressedThreshold (float height)
{
	SERVER_IMPL->morphlingComms.setMorphlingPressedThreshold(height);
}

void Server::setPeakCurrent (float value)
{
	SERVER_IMPL->morphlingComms.setPeakCurrent(value);
}

void Server::setMeanCurrent (float value)
{
	SERVER_IMPL->morphlingComms.setMeanCurrent(value);
}

void Server::switchToSoft (void)
{
	SERVER_IMPL->morphlingComms.setPeakCurrent(0.06);
	SERVER_IMPL->morphlingComms.setMeanCurrent(0.350386918);
}

void Server::switchToHard (void)
{
	SERVER_IMPL->morphlingComms.setPeakCurrent(1.4);
	SERVER_IMPL->morphlingComms.setMeanCurrent(0.48);
}

void Server::setControlPeriod (unsigned period)
{
	SERVER_IMPL->morphlingComms.setControlPeriod(period);
}

void Server::setFrametime(float frametime)
{
	SERVER_IMPL->morphlingComms.setFrametime(frametime);
}

void Server::startSineWave (void)
{
	SERVER_IMPL->morphlingComms.startSineWave();
}

void Server::stopSineWave (void)
{
	SERVER_IMPL->morphlingComms.stopSineWave();
}

void Server::setSineWaveParams (float periodLength, float movementSpeed)
{
	SERVER_IMPL->morphlingComms.setSineWaveParams(periodLength, movementSpeed);
}

void Server::startup (bool block)
{
	SERVER_IMPL->morphlingComms.startup(block);
}

void Server::shutdown (void)
{
	SERVER_IMPL->morphlingComms.shutdown();
}

void Server::raiseMPE (void)
{
	MorphlingPressed(this, gcnew MorphlingPressedEventArgs);
}


////
// RobotArm::RobotArmStateChangedEventArgs

RobotArm::StateChangedEventArgs::StateChangedEventArgs(
	StateChange stateChange, bool success
)
	: newStateValue(stateChange), successValue(success)
{}

RobotArm::StateChangedEventArgs::~StateChangedEventArgs() {}

RobotArm::StateChangedEventArgs::!StateChangedEventArgs() {}


////
// RobotArm

// The event callback for translating robot arm operations to .NET events
class RobotArmOpsEventTranslator : public RobotArmComms::RobotArmOperationCallback
{
private:
	gcroot<RobotArm^> ra;
public:
	RobotArmOpsEventTranslator(RobotArm^ ra) : ra(ra) {};

	virtual void done (RobotArmComms::RobotArmOperationType operation, bool success)
	{
		RobotArm::StateChangedEventArgs^ ea =
			gcnew RobotArm::StateChangedEventArgs(
				operation == RobotArmComms::RA_EXTEND ?
					RobotArm::StateChange::RA_EXTENDED :
					RobotArm::StateChange::RA_RETRACTED,
				success
			);
		ra->raiseRAE(ea);
	}
};

// Implementation details
struct RobotArm_impl
{
	RobotArmComms raComms;
	RobotArmOpsEventTranslator raTranslator;

	RobotArm_impl(const TCHAR *address, unsigned port, RobotArm^ e)
		: raComms(address, port), raTranslator(e)
	{
		raComms.setOperationCompleteCallback(raTranslator);
	}
};
#define ROBOTARM_IMPL ((RobotArm_impl*)pimpl)

RobotArm::RobotArm(System::String^ serverAddress, unsigned port)
	: pimpl(nullptr)
{
	// Convert managed data
	const cli::pin_ptr<const TCHAR> addressString = PtrToStringChars(serverAddress);

	// Allocate implementation instance
	pimpl = new RobotArm_impl(addressString, port, this);
	if (pimpl == nullptr)
		throw gcnew System::OutOfMemoryException(L"[MorphlingAPI::RobotArm] Failed to"
		                                         L" allocate class instance.");
}

RobotArm::~RobotArm()
{
	this->!RobotArm();
}

RobotArm::!RobotArm()
{
	if (pimpl)
	{
		delete ROBOTARM_IMPL;
		pimpl = nullptr;
	}
}

bool RobotArm::ping (void)
{
	return ROBOTARM_IMPL->raComms.ping();
}

void RobotArm::extend (bool block)
{
	ROBOTARM_IMPL->raComms.extend(block);
}

void RobotArm::retract (bool block)
{
	ROBOTARM_IMPL->raComms.retract(block);
}

void RobotArm::reloadConfig (void)
{
	ROBOTARM_IMPL->raComms.reloadConfig();
}

void RobotArm::enableControl (bool enabled)
{
	ROBOTARM_IMPL->raComms.enableControl(enabled);
}

void RobotArm::raiseRAE (StateChangedEventArgs^ eventArgs)
{
	StateChanged(this, eventArgs);
}
