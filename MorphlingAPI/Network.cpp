
//////
//
// Includes
//

// C++ STL and Boost
#if (_MSC_VER < 1910)
	#include <boost/any.hpp>
#else
	#include <any>
#endif
#include <vector>
#include <string>
#include <sstream>
#include <memory>
#include <exception>
#include <stdexcept>

// Windows API
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <winsock2.h>
#include <WS2tcpip.h>

// Local includes
#include "Utils.h"

// Implemented header
#include "Network.h"



//////
//
// Namespaces
//

// Implemented namespace
using namespace Morphling;



//////
//
// Module-private types
//

// WHY MICROSOFT, WHY????? :(
#if defined(UNICODE) || defined(_UNICODE)
	typedef ADDRINFOW AddrInfo;
#else
	typedef ADDRINFO AddrInfo;
#endif

// RAII-augmented AddrInfo
typedef RAII<AddrInfo, FreeAddrInfo> AddrInfoRAII;



//////
//
// Module initialization and globals
//

// No "storage class specifier 'register' is deprecated" warnings
#pragma warning (disable : 5033)

// [BEGIN] Module-private global namespace
namespace {

// Takes care of intialization on dll load
struct Initializer
{
	Initializer()
	{
		WSADATA wsaData; // <- not further inspected currently
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
			throw gcnew System::Exception(
				L"[MorphlingAPI::Startup] Unable to initialize Winsocks"
				L" - WSAStartup() failed!"
			);
	}
	~Initializer()
	{
		//WSACleanup();
	}
} initializer;

// [END] Module-private global namespace
}



//////
//
// Class implementation
//

////
// IPTransport

struct IP_impl
{
	SOCKET s;

	IP_impl(const String &peerIpAddress, unsigned port, SOCKET s) : s(s)
	{
		if (s == INVALID_SOCKET || s == -1)
			// Socket could not be created. Compile appropriate error description and
			// then throw up.
			throw gcnew System::Exception(formatManagedExceptionMsg(
				WSAGetLastError(), L"MorphlingAPI::IP", L"Unable to create socket."
			));

		SOCKADDR_IN peerAddress;
		peerAddress.sin_family = AF_INET;
		peerAddress.sin_port = htons(port);
		std::fill_n(peerAddress.sin_zero, 8, char(0));
		INT result = InetPton(
			peerAddress.sin_family, peerIpAddress.c_str(), &peerAddress.sin_addr
		);
		if (result == 0)
			throw gcnew System::ArgumentException(formatManagedExceptionMsg(
				L"MorphlingAPI::IP", L"Unable parse host IP address - invalid format.",
				{{ L"peerIpAddress", peerIpAddress }}
			));
		else if (result != 1)
			// An error occurred. Compile appropriate description and then throw up.
			throw gcnew System::Exception(formatManagedExceptionMsg(
				WSAGetLastError(), L"MorphlingAPI::IP", L"Unable parse host IP address"
				L" - InetPton() failed.", {{ L"peerIpAddress", peerIpAddress }}
			));

		// Ready things for communication with the peer
		DWORD timeout = 1;
		setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(DWORD));
		result = connect(s, (SOCKADDR*)&peerAddress, sizeof(SOCKADDR_IN));
		if (result != 0)
			// An error occurred. Compile appropriate description and then throw up.
			throw gcnew System::Exception(formatManagedExceptionMsg(
				WSAGetLastError(), L"MorphlingAPI::IP", L"Unable to connect to host.",
				{{ L"peerIpAddress", peerIpAddress }, { L"port", port }}
			));
		timeout = 0;
		setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(DWORD));
	}
	~IP_impl()
	{
		if (s != INVALID_SOCKET)
		{
			closesocket(s);
			s = INVALID_SOCKET;
		}
	}
};
#define IP_IMPL ((IP_impl*)pimpl)

IPTransport::IPTransport(const String &peerIpAddress, unsigned port, void* sHandle)
{
	// Allocate implementation instance
	pimpl = new IP_impl(peerIpAddress, port, (SOCKET)sHandle);
	if (pimpl == nullptr)
		throw gcnew System::OutOfMemoryException(L"[MorphlingAPI::IP] Failed to allocate class instance.");
}

IPTransport::~IPTransport()
{
	if (pimpl)
	{
		delete IP_IMPL;
		pimpl = nullptr;
	}
}

String IPTransport::resolveHost(const String &host)
{
	// Try to resolve host to IP address
	SOCKADDR_IN ipAddress; ipAddress.sin_family = AF_INET;
	INT address_type = InetPton(ipAddress.sin_family, host.c_str(), &ipAddress.sin_addr);

	// Evaluate the results
	switch (address_type)
	{
	case 1:
		// No name resolution necessary, serverAdress already contained an IP address
		return host;

	case 0: {
		// Try resolving DNS name
		AddrInfo hints = {0};
		AddrInfoRAII resolved;
		hints.ai_family = ipAddress.sin_family;
		INT result = GetAddrInfo(host.c_str(), nullptr, &hints, resolved.address());
		if (result != 0)
			// An error occurred. Compile appropriate description and then throw up.
			throw gcnew System::Exception(formatManagedExceptionMsg(
				result, L"MorphlingAPI::IP",
				L"Unable to obtain host IP address - name lookup failed.",
				{{ L"host", host }}
		));

		// Check if we can use the resolution
		if (resolved.thing->ai_addr->sa_family != ipAddress.sin_family ||
			resolved.thing->ai_addr->sa_data[0] || resolved.thing->ai_addr->sa_data[1])
			// We don't support this kind of address
			throw gcnew System::NotSupportedException(formatManagedExceptionMsg(
				L"MorphlingAPI::IP", L"Unable to obtain host IP address - name lookup"
				L" returned unsupported address format or protocol family.",
				{{ L"<family_indicator>", resolved.thing->ai_addr->sa_family },
				 { L"<format_indicator>", (const void*)size_t(
					BYTE(resolved.thing->ai_addr->sa_data[1]) |
					BYTE(resolved.thing->ai_addr->sa_data[0]) << 8
				   ) },
				 { L"host", host }}
		));

		// Commit found ip address
		ipAddress.sin_addr.S_un.S_un_b.s_b1 = resolved.thing->ai_addr->sa_data[2];
		ipAddress.sin_addr.S_un.S_un_b.s_b2 = resolved.thing->ai_addr->sa_data[3];
		ipAddress.sin_addr.S_un.S_un_b.s_b3 = resolved.thing->ai_addr->sa_data[4];
		ipAddress.sin_addr.S_un.S_un_b.s_b4 = resolved.thing->ai_addr->sa_data[5];
		break;
	}

	case -1:
		// An error occurred. Compile appropriate description and then throw up.
		throw gcnew System::ArgumentException(formatManagedExceptionMsg(
			WSAGetLastError(), L"MorphlingAPI::IP",
			L"Unable to obtain host IP address - host specifier could not be parsed.",
			{ { L"host", host } }
		));

	default:
		throw gcnew System::Exception(L"[MorphlingAPI::IP] Unable to resolve host"
			L" address - internal error.");
	}

	// Convert back to string
	TCHAR buf[INET_ADDRSTRLEN];
	if (!InetNtop(ipAddress.sin_family, &ipAddress.sin_addr, buf, INET_ADDRSTRLEN))
		throw gcnew System::Exception(formatManagedExceptionMsg(
			WSAGetLastError(), L"MorphlingAPI::IP", L"Unable to obtain host IP address"
			L" - InetNtop() could not parse IP address blob returned by GetAddrInfo().",
			{{ L"ipAddress.sin_addr", (void*)(size_t)ipAddress.sin_addr.S_un.S_addr }}
	));
	// ...and done!
	return buf;
}


////
// UDP

UDP::UDP(const String &peerIpAddress, unsigned port)
	: IPTransport(peerIpAddress, port, (void*)socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))
{
}

UDP::~UDP()
{
}

void UDP::send (const void *buffer, unsigned length)
{
	INT sent = ::send(IP_IMPL->s, (const char*)buffer, length, NULL);
	if (sent < 1)
		// An error occurred. Compile appropriate description and then throw up.
		throw gcnew System::Exception(formatManagedExceptionMsg(
			WSAGetLastError(), L"MorphlingAPI::UDP", L"Unable to send datagram to peer"
			L" - socket error."
		));
	else if (unsigned(sent) != length)
		// UDP sends are supposed to be atomic...
		throw gcnew System::Exception(formatManagedExceptionMsg(
			L"MorphlingAPI::UDP", L"Unable to send datagram to peer - payload was cut"
			L" off mid-send by the socket API.",
			{{ L"<bytes sent>", sent }, { L"<payload length>", length }}
		));
}

bool UDP::recv(void *buffer, unsigned length, bool dontBlock)
{
	// Shortcut to save one indirection per member access
	register IP_impl *impl = IP_IMPL;

	// Oversized temp buffer to detect truncation
	char tmpbuf [4096];

	// Peek if in non-blocking mode
	if (dontBlock)
	{
		// Peak if datagram available
		fd_set socketSet;
		socketSet.fd_count = 1;
		socketSet.fd_array[0] = impl->s;
		const timeval _1us = { 0, 1 };
		int result = select(0, &socketSet, nullptr, nullptr, &_1us);
		if (result == SOCKET_ERROR)
			// An error occurred. Compile appropriate description and then throw up.
			throw gcnew System::Exception(formatManagedExceptionMsg(
				WSAGetLastError(), L"MorphlingAPI::UDP", L"Unable to receive datagrams from"
				L" peer - socket error."
			));
		if (result == 0)
			return false;
	}

	// Receive a datagram
	int received; received = ::recv(impl->s, tmpbuf, 4096, NULL);
	if (received < 1)
		// An error occurred. Compile appropriate description and then throw up.
		throw gcnew System::Exception(formatManagedExceptionMsg(
			WSAGetLastError(), L"MorphlingAPI::UDP", L"Unable to receive datagrams from"
			L" peer - socket error."
		));
	// Check for truncation
	if (received < int(length))
		throw gcnew System::Exception(formatManagedExceptionMsg(
			L"MorphlingAPI::UDP", L"Unable to receive datagram from peer - payload was"
			L" cut off by the socket API.",
			{{ L"<bytes received>", received }, { L"<bytes expected>", length }}
		));
	if (received-int(length) > 0)
		throw gcnew System::Exception(formatManagedExceptionMsg(
			L"MorphlingAPI::UDP", L"Unable to receive datagram from peer - payload"
			L" exceeded expected size.",
			{{ L"<bytes received>", received }, { L"<bytes expected>", length }}
		));

	// Copy to target buffer
	std::copy_n(tmpbuf, length, (char*)buffer);
	return true;
}


////
// TCP

TCP::TCP(const String &peerIpAddress, unsigned port)
	: IPTransport(peerIpAddress, port, (void*)socket(AF_INET, SOCK_STREAM, IPPROTO_TCP))
{
}

TCP::~TCP()
{
}

void TCP::send(const void *buffer, unsigned length)
{
	INT sent = ::send(IP_IMPL->s, (const char*)buffer, length, NULL);
	if (sent < 1)
		// An error occurred. Compile appropriate description and then throw up.
		throw gcnew System::Exception(formatManagedExceptionMsg(
			WSAGetLastError(), L"MorphlingAPI::TCP", L"Unable to send data to host"
			L" - socket error."
		));
	else if (unsigned(sent) != length)
		// We don't support fragmented packets yet...
		throw gcnew System::Exception(formatManagedExceptionMsg(
			L"MorphlingAPI::TCP", L"Unable to send data to host - payload was cut"
			L" off mid-send by the socket API.",
			{{ L"<bytes sent>", sent }, { L"<payload length>", length }}
	));
}

bool TCP::recv(void *buffer, unsigned length, bool dontBlock)
{
	// Shortcut to save one indirection per member access
	register IP_impl *impl = IP_IMPL;

	// Peek if in non-blocking mode
	if (dontBlock)
	{
		// Peak if datagram available
		fd_set socketSet;
		socketSet.fd_count = 1;
		socketSet.fd_array[0] = impl->s;
		const timeval _1us = { 0, 1 };
		int result = select(0, &socketSet, nullptr, nullptr, &_1us);
		if (result == SOCKET_ERROR)
			// An error occurred. Compile appropriate description and then throw up.
			throw gcnew System::Exception(formatManagedExceptionMsg(
				WSAGetLastError(), L"MorphlingAPI::TCP", L"Unable to receive data from"
				L" host - socket error."
			));
		if (result == 0)
			return false;
	}

	// Receive until the whole message is in
	unsigned processed = 0;
	do
	{
		unsigned remaining = length-processed;
		int received = ::recv(impl->s, ((char*)buffer)+processed, remaining, NULL);
		if (received < 0)
			// An error occurred. Compile appropriate description and then throw up.
			throw gcnew System::Exception(formatManagedExceptionMsg(
				WSAGetLastError(), L"MorphlingAPI::TCP", L"Unable to receive data"
				L" from host - socket error."
			));

		if (unsigned(received) <= remaining)
			// Commit everything
			processed += received;
		else
			// Commit what we can
			processed = length;
	}
	while (processed < length);

	// Done!
	return true;
}
