
#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__


//////
//
// Namespaces
//

// Implemented namespace
namespace Morphling {



//////
//
// Classes
//

/**
 * @brief
 *		Central class wrapping the functionality of the morphling communication service.
 */
class MorphlingComms
{

public:

	////
	// Exported types

	/**
	 * @brief
	 *		Callback functor for asynchronous detection of the "morphling pressed down"
	 *		interaction.
	 */
	struct MorphlingPressedCallback
	{
		////
		// Object construction / destruction

		/** @brief Virtual destructor - causes V-table creation. */
		virtual ~MorphlingPressedCallback();


		////
		// Methods

		/**
		 * @brief
		 *		Called when the morphling was detected to have moved below the "pressed
		 *		down" threshold.
		 */
		virtual void pressed (void) = 0;
	};


protected:

	////
	// Data members

	/** @brief Implementation handle. */
	void* pimpl;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs the instance with the morphling control service at the given
	 *		address and port.
	 *
	 * @param address
	 *		The server address hosting the morphling service, given either by name (which
	 *		will be resolved using system DNS resolution) or by IPv4 address. Should be a
	 *		a null-terminated character array of appropriate width, depending on the
	 *		compiler character encoding settings (e.g. use TCHAR on Windows/Visual C++).
	 * @param port
	 *		The TCP port on which the morphling service is reachable on the server.
	 */
	MorphlingComms(const void *address, unsigned port);

	/** @brief The destructor. */
	~MorphlingComms();


	////
	// Methods

	/**
	 * @brief Set target height for given actuator.
	 *
	 * @param actuator
	 *		ID of the actuator to set the target height for.
	 * @param height
	 *		The height, in mm, that the actuator should position itself at.
	 */
	void setHeight (unsigned actuator, double height);

	/**
	 * @brief
	 *		Set target height for the given actuators. This is more efficient than
	 *		individual calls to @ref #setHeight due to consolidated synchronization
	 *		overhead.
	 *
	 * @param IDs
	 *		IDs of the actuators that should get their target heights set.
	 * @param heights
	 *		The target heights, in mm, that the actuators should position themselves at.
	 */
	void setHeights (
		const std::vector<unsigned> &IDs, const std::vector<double> &heights
	);

	/**
	 * @brief
	 *		Set target height for all actuators. This is more efficient than individual
	 *		calls to @ref #setHeight or @ref #setHeights with subsets of actuators due to
	 *		consolidated synchronization overhead.
	 *
	 * @param pose
	 *		The target heights for all actuators, in mm, that they should position
	 *		themselves at.
	 */
	void setPose (const std::vector<double> &pose);

	/**
	 * @brief Retrieves the current position of all actuators.
	 *
	 * @param positions_out
	 *		Pointer to an <tt>std::vector</tt> that should receive the positions. Data
	 *		already in the vector will be lost.
	 */
	void getPose (std::vector<double> *pose_out);

	/**
	 * @brief
	 *		Sets the callback for notification of detected "morphling pressed down"
	 *		interactions.
	 *
	 * @param callback
	 *		Pointer to the callback functor. Passing @c nullptr disables the callback.
	 */
	void setMorphlingPressedCallback (MorphlingPressedCallback &callback);

	/**
	 * @brief
	 *		Sets the actuator height that should signify a "pressed down" state. The
	 *		default is @c 0, which basically disables the detection of the interation.
	 *
	 * @param height
	 *		The actuator height, in millimeters, below which the morphling is considered
	 *		to be "pressed down". Set this to @c 0 or a negative value to effectively
	 *		disable detection.
	 */
	void setMorphlingPressedThreshold (double height);

	/**
	 * @brief Set peak current for actuators.
	 *
	 * @param value
	 *		The current, in amperes, the actuators are allowed to peak to when moving.
	 */
	void setPeakCurrent (double value);

	/**
	 * @brief Set mean current for actuators.
	 *
	 * @param value
	 *		The current, in amperes, the actuators should maintain on average.
	 */
	void setMeanCurrent (double value);

	/**
	 * @brief Set the cycle period of the control thread.
	 *
	 * @param period
	 *		The period, in microseconds, between morphling communication ticks. Defaults
	 *		to 5000μs / 200Hz.
	 */
	void setControlPeriod (unsigned period);

	/**
	 * @brief
	 *		Set the current frame time of your calling application. Enables interpolation
	 *		between subsequent actuator position change requests for smoothness up to
	 *		the resolution implied by @ref #setControlPeriod . Useful for applications
	 *		that run at much less than 200Hz.
	 *
	 * @param frametime
	 *		The current frame time, in seconds, of your application. Setting this to
	 *		@c 0 (the default) or a negative value disables interpolation.
	 */
	void setFrametime (double frametime);

	/** @brief Initiate a sine wave pattern. */
	void startSineWave (void);

	/** @brief Stop any running sine wave pattern. */
	void stopSineWave (void);

	/**
	 * @brief Sets the parameters of the sine wave sampled by @ref #startSineWave .
	 *
	 * @param periodLength
	 *		The length, in millimeters, of one sine period.
	 * @param movementSpeed
	 *		The speed, in millimeters per second, that the morphling should move over the
	 *		sine landscape.
	 */
	void setSineWaveParams (double periodLength, double movementSpeed);

	/**
	 * @brief
	 *		Initiates the startup procedure on the morphling. It will take the morphling
	 *		about 15 seconds until it can actually process commands.
	 *
	 * @param block
	 *		Indicates whether the call should block until the morphling indicates that it
	 *		is ready.
	 */
	void startup (bool block);

	/**
	 * @brief
	 *		Initiates the shutdown procedure on the morphling. It will be unable to
	 *		process new commands until @ref #initialize is called.
	 */
	void shutdown (void);
};

/**
 * @brief
 *		Central class wrapping the functionality of the robot arm communication service.
 */
class RobotArmComms
{

public:

	////
	// Exported types

	/**
	 * @brief Enumerates all robot arm operations that a callback can be registered for.
	 */
	enum RobotArmOperationType
	{
		/**
		 * @brief
		 *		Indicates that the performed operation was the extension of the robot
		 *		arm.
		 */
		RA_EXTEND,

		/**
		 * @brief
		 *		Indicates that the performed operation was the retraction of the robot
		 *		arm.
		 */
		RA_RETRACT
	};

	/** @brief Callback functor for asynchronous robot arm operations. */
	struct RobotArmOperationCallback
	{
		////
		// Object construction / destruction

		/** @brief Virtual destructor - causes V-table creation. */
		virtual ~RobotArmOperationCallback();


		////
		// Methods

		/**
		 * @brief Called when the robot arm operation is considered over.
		 *
		 * @param operation
		 *		The operation that was being performed.
		 * @param success
		 *		@c true when the robot arm communicator determined that the operation was
		 *		performed successfully, @c false if this could not be verified (e.g.
		 *		because of a timeout).
		 */
		virtual void done (RobotArmOperationType operation, bool success) = 0;
	};


protected:

	////
	// Data members

	/** @brief Implementation handle. */
	void* pimpl;


public:

	////
	// Object construction / destruction

	/**
	 * @brief
	 *		Constructs the instance with the robot arm control service at the given
	 *		address and port.
	 *
	 * @param address
	 *		The server address hosting robot arm control service, given either by name
	 *		(which will be resolved using system DNS resolution) or by IPv4 address.
	 *		Should be a a null-terminated character array of appropriate width, depending
	 *		on the compiler character encoding settings (e.g. use TCHAR on Windows/Visual
	 *		C++).
	 * @param port
	 *		The TCP port on which the robot arm service is reachable on the server.
	 */
	RobotArmComms(const void *address, unsigned port);

	/** @brief The destructor. */
	~RobotArmComms();


	////
	// Methods

	/**
	 * @brief
	 *		Tests if the robot control service reacts to commands. This call will block
	 *		until a result is obtained.
	 *
	 * @return
	 *		@c true if the robot control service responded with a ping-back, @c false if
	 *		there was no reaction withing the specified timeout.
	 *
	 * @param timeout
	 *		The timeout, in milliseconds, when the robot control service is considered
	 *		unresponsive. Passing @c 0 is an alias for passing @c 5000 , i.e. 5 seconds.
	 */
	bool ping (unsigned timeout=0);

	/**
	 * @brief
	 *		Tells the robot control service to extend the robot arm.
	 *
	 * @param block
	 *		Indicates whether the call should block until the robot control service
	 *		indicates that the arm is completely extended.
	 */
	void extend (bool block);

	/**
	 * @brief
	 *		Tells the robot control service to retract the robot arm.
	 *
	 * @param block
	 *		Indicates whether the call should block until the robot control service
	 *		indicates that the arm is completely retracted.
	 */
	void retract (bool block);

	/**
	 * @brief Tells the robot control service to reload the main configuration from disk.
	 */
	void reloadConfig (void);

	/**
	 * @brief
	 *		Tells the robot control service to start or stop the force control loop.
	 *
	 * @param enabled
	 *		State of the force control loop - @c true for on and @c false for off.
	 */
	void enableControl (bool enabled);

	/**
	 * @brief
	 *		Sets the callback for notification of completed robot arm operations (extend,
	 *		retract).
	 *
	 * @param callback
	 *		Pointer to the callback functor. Passing @c nullptr disables the callback.
	 */
	void setOperationCompleteCallback(RobotArmOperationCallback &callback);
};


// Close implemented namespace
}


#endif // ifndef __COMMUNICATION_H__
