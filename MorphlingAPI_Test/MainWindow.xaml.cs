﻿
// .NET Framework
using System;
using System.Collections;
using System.Timers;
using System.Threading;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

// Implementation
namespace MorphlingAPI_Test
{

	public class SineParams : INotifyPropertyChanged
	{
		private Single _period, _amplitude, _localFreq;
		public event PropertyChangedEventHandler PropertyChanged;

		public SineParams (Single period, Single amplitude, Single localFreq)
		{
			_period = period;
			_amplitude = amplitude;
			_localFreq = localFreq;
		}

		public Single period
		{
			get { return _period; }
			set { _period = value; Notify("period"); }
		}
		public Single amplitude
		{
			get { return _amplitude; }
			set { _amplitude = value; Notify("amplitude"); }
		}
		public Single localFreq
		{
			get { return _localFreq; }
			set { _localFreq = value; Notify("localFreq"); }
		}
		public void Notify (string propertyName)
		{
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
	}

	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		protected Morphling.Server morphling;
		protected Morphling.RobotArm robotArm;
		protected System.Timers.Timer timer_curPos, timer_sinUpdate;
		protected delegate void updateMorphlingPosLabelsDelegate(ArrayList labelContents);
		protected SineParams sineParams = new SineParams(2000, 60, 30);
		protected bool sineRunning;
		protected Single sinePos;

		public MainWindow()
		{
			// WPF-mandated
			InitializeComponent();
			this.DataContext = sineParams;

			// Obtain reference to morphling server
			morphling = ((App)Application.Current).morphling;

			// Set initial parameters
			// - set hardness to initially medium
			currentSlider.Value = 250;
			currentSlider_valueChanged(null, null);
			// - set cycle period to default 5ms
			timingSlider.Value = 5000;
			timingSlider_valueChanged(null, null);

			// Set our sine wave data model PropertyChanged event
			sineParams.PropertyChanged += sineTextboxes_valueChanged;

			// Set up periodic updates of morphling state
			timer_curPos = new System.Timers.Timer(100);
			timer_curPos.AutoReset = true;
			timer_curPos.Elapsed += updateMorphlingStateEvent;
			timer_curPos.Start();

			// Set up timer for local sine wave generation
			timer_sinUpdate = new System.Timers.Timer(1000.0 / sineParams.localFreq);
			timer_sinUpdate.AutoReset = true;
			timer_sinUpdate.Elapsed += updateSineWave;
			sineRunning = false;
			sinePos = 0;

			// Subscribe to the morphling button-"press" event and configure its
			// "button" threshold
			morphling.MorphlingPressed += onMorphlingPressed;
			morphling.setMorphlingPressedThreshold(20);
		}

		~MainWindow()
		{
			// Release reference to morphling server
			morphling = null;
		}

		void onClose (object sender, CancelEventArgs e)
		{
			// Stop updating morphling state and/or sine pattern
			timer_curPos.Stop();
			timer_sinUpdate.Stop();
		}

		// Morphling state updater
		private void updateMorphlingPosLabels (ArrayList labelContents)
		{
			act0pos.Content = labelContents[0];
			act1pos.Content = labelContents[1];
			act2pos.Content = labelContents[2];
			act3pos.Content = labelContents[3];
			act4pos.Content = labelContents[4];
		}
		private void updateMorphlingStateEvent (Object sender, ElapsedEventArgs e)
		{
			// Update actuator positions
			Single[] positions = morphling.getPose();
			ArrayList texts = new ArrayList(positions.Length);
			for (int i = 0; i < positions.Length; i++)
				texts.Add(positions[i].ToString("#.000") + " mm");
			this.Dispatcher.Invoke(
				new updateMorphlingPosLabelsDelegate(updateMorphlingPosLabels), texts
			);
		}

		// Sine wave update timer
		Single sine (Single s)
		{
			return ((Single)Math.Sin(s*(2*Math.PI)/200.0)) * 10.0f + 40.0f;
		}
		private void updateSineWave (Object sender, ElapsedEventArgs e)
		{
			// Update sine position
			Single frametime = (1.0f / sineParams.localFreq),
				   moveSpeed = 200.0f * 1000.0f / sineParams.period;
			sinePos += frametime * moveSpeed;
			if (sinePos > 200.0f)
				sinePos -= 200.0f;

			// Control actuators
			morphling.setPose(new Single [] {
				sine(sinePos + morphling.getFootpoint(0)[0]),
				sine(sinePos + morphling.getFootpoint(1)[0]),
				sine(sinePos + morphling.getFootpoint(2)[0]),
				sine(sinePos + morphling.getFootpoint(3)[0]),
				sine(sinePos + morphling.getFootpoint(4)[0])
			});
		}

		// Morphling events
		private void onMorphlingPressed (
			object sender, Morphling.Server.MorphlingPressedEventArgs e
		)
		{
			MessageBox.Show("Morphling was pressed!");
		}

		// Robot arm events
		private void onRobotArmStateChanged(
			object sender, Morphling.RobotArm.StateChangedEventArgs e
		)
		{
			if (e.newState == Morphling.RobotArm.StateChange.RA_EXTENDED && e.success)
				MessageBox.Show("Robot arm extended successfully!");
			else if (e.newState == Morphling.RobotArm.StateChange.RA_EXTENDED)
				MessageBox.Show("Unable to confirm robot arm extension!");
			else if (e.newState == Morphling.RobotArm.StateChange.RA_RETRACTED && e.success)
				MessageBox.Show("Robot arm retracted successfully!");
			else if (e.newState == Morphling.RobotArm.StateChange.RA_RETRACTED)
				MessageBox.Show("Unable to confirm robot arm retraction!");
			this.Dispatcher.Invoke(() => {
				pingButton.IsEnabled = true;
				extendButton.IsEnabled = true;
				retractButton.IsEnabled = true;
				startCtrlButton.IsEnabled = true;
				stopCtrlButton.IsEnabled = true;
			});
		}

		// Common event handlers
		private void textBox_enterKey(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter)
			{
				TextBox box = (TextBox)sender;
				DependencyProperty prop = TextBox.TextProperty;

				BindingExpression binding = BindingOperations.GetBindingExpression(box, prop);
				if (binding != null) { binding.UpdateSource(); }
			}
		}

		// Actuators to high position buttons
		private void buttonAllHigh_click(object sender, RoutedEventArgs e)
		{
			morphling.setPose(new Single[] { 80.0f, 80.0f, 80.0f, 80.0f, 80.0f });
		}
		private void button1high_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(0, 80);
		}
		private void button2high_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(1, 80);
		}
		private void button3high_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(2, 80);
		}
		private void button4high_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(3, 80);
		}
		private void button5high_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(4, 80);
		}

		// Actuators to middle position buttons
		private void buttonAllMid_click(object sender, RoutedEventArgs e)
		{
			morphling.setPose(new Single[] { 40.0f, 40.0f, 40.0f, 40.0f, 40.0f });
		}
		private void button1mid_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(0, 40);
		}
		private void button2mid_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(1, 40);
		}
		private void button3mid_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(2, 40);
		}
		private void button4mid_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(3, 40);
		}
		private void button5mid_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(4, 40);
		}

		// Actuators to low position buttons
		private void buttonAllLow_click(object sender, RoutedEventArgs e)
		{
			morphling.setPose(new Single[] { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f });
		}
		private void button1low_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(0, 0);
		}
		private void button2low_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(1, 0);
		}
		private void button3low_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(2, 0);
		}
		private void button4low_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(3, 0);
		}
		private void button5low_click(object sender, RoutedEventArgs e)
		{
			morphling.setHeight(4, 0);
		}

		// Initialize button
		private void buttonReinit_click(object sender, RoutedEventArgs e)
		{
			buttonReinit.IsEnabled = false;
			try {
				morphling.startup(true);
			}
			catch (Exception ex) {
				// Just rethrow - we're just catching this exception for the finalizer
				throw ex;
			}
			finally { buttonReinit.IsEnabled = true; }
		}

		// Sine wave controls
		private void buttonSinStart_click (object sender, RoutedEventArgs e)
		{
			sineRunning = true;
			if ((bool)localSineToggle.IsChecked)
			{
				morphling.setFrametime(1.0f / sineParams.localFreq);
				timer_sinUpdate.Start();
			}
			else
				morphling.startSineWave();
		}
		private void sineTextboxes_valueChanged (object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "period")
			{
				Single periodLen = 200, // We actually keep this fixed
				       moveSpeed = periodLen * 1000.0f/sineParams.period;
				morphling.setSineWaveParams(periodLen, moveSpeed);
			}
		}
		private void buttonSinStop_click (object sender, RoutedEventArgs e)
		{
			if ((bool)localSineToggle.IsChecked)
				timer_sinUpdate.Stop();
			else
				morphling.stopSineWave();
			sineRunning = false;
		}
		private void localSineToggle_checked(object sender, RoutedEventArgs e)
		{
			if (sineRunning)
			{
				morphling.stopSineWave();
				timer_sinUpdate.Start();
			}
		}
		private void localSineToggle_unchecked (object sender, RoutedEventArgs e)
		{
			if (sineRunning)
			{
				timer_sinUpdate.Stop();
				morphling.startSineWave();
			}
		}
		private void localSineInterpolToggle_checked(object sender, RoutedEventArgs e)
		{
			// Prevent sends to morphling from spurious GUI-initialization events
			if (IsInitialized)
				morphling.setFrametime(1.0f / sineParams.localFreq);
		}
		private void localSineInterpolToggle_unchecked(object sender, RoutedEventArgs e)
		{
			// Prevent sends to morphling from spurious GUI-initialization events
			if (IsInitialized)
				morphling.setFrametime(0);
		}

		// Current slider controls
		private void currentSlider_valueChanged (object sender, RoutedEventArgs e)
		{
			// Prevent sends to morphling from spurious GUI-initialization events
			if (IsInitialized)
			{
				// Prevent decimal digits for GUI usability reasons
				currentSlider.Value = Math.Round(currentSlider.Value, 0);

				// Send to Morphling
				Single currentMean = (Single)(
					  0.350
					+ 0.150 *   (currentSlider.Value - currentSlider.Minimum)
							  / (currentSlider.Maximum - currentSlider.Minimum)
				);
				Single currentPeak = ((Single)currentSlider.Value) / 1000;
				morphling.setMeanCurrent(currentMean);
				morphling.setPeakCurrent(currentPeak);

				// Done!
				return;
			}
		}

		// Timing slider controls
		private void timingSlider_valueChanged (object sender, RoutedEventArgs e)
		{
			// Prevent sends to morphling from spurious GUI-initialization events
			if (IsInitialized)
			{
				// Prevent decimal digits for GUI usability reasons
				timingSlider.Value = Math.Round(timingSlider.Value, 0);

				// Send to Morphling
				morphling.setControlPeriod((uint)timingSlider.Value);

				// Done!
				return;
			}
		}

		// Robot arm controls
		private void ensureMorhplingRetracted ()
		{
			// Stop any running sine animation
			buttonSinStop_click(null, null);

			// Make sure the morphling fingers are retracted before returning
			Single[] pose = morphling.getPose(), zeroPose = new float[] {0, 0, 0, 0, 0};
			while (   pose[0]>3.5f || pose[1]>3.5f || pose[2]>3.5f || pose[3]>3.5f
			       || pose[4]>3.5f)
			{
				morphling.setPose(zeroPose);
				Thread.Sleep(1); // Try not to spin the CPU too much...
				pose = morphling.getPose();
			}
		}
		private void connectButton_click (object sender, RoutedEventArgs e)
		{
			if (robotArm == null)
			{
				// Connect to robot control
				try {
					robotArm = ((App)Application.Current).robotArm =
						new Morphling.RobotArm("192.168.0.26", 8800);
					connectButton.Content = "disconnect";
				}
				catch (Exception ex) {
					// We want program execution to continue without robot arm control in
					// case we can't connect
					robotArm = ((App)Application.Current).robotArm = null;
					MessageBox.Show(ex.ToString());
					return;
				}
				// Subscribe to the robot arm state changed event
				robotArm.StateChanged += onRobotArmStateChanged;
				pingButton.IsEnabled = true;
				extendButton.IsEnabled = true;
				retractButton.IsEnabled = true;
				reloadCfgButton.IsEnabled = true;
				startCtrlButton.IsEnabled = true;
				stopCtrlButton.IsEnabled = true;
			}
			else
			{
				robotArm = null;
				((App)Application.Current).robotArm.Dispose();
				((App)Application.Current).robotArm = null;
				pingButton.IsEnabled = false;
				extendButton.IsEnabled = false;
				retractButton.IsEnabled = false;
				reloadCfgButton.IsEnabled = false;
				startCtrlButton.IsEnabled = false;
				stopCtrlButton.IsEnabled = false;
				connectButton.Content = "connect";
			}
		}
		private void pingButton_click (object sender, RoutedEventArgs e)
		{
			if (robotArm.ping())
				MessageBox.Show("Robot control service connection OK.");
			else
				MessageBox.Show("Robot control service did not respond to ping.");
		}
		private void extendButton_click (object sender, RoutedEventArgs e)
		{
			// Disable relevant controls during the operation
			pingButton.IsEnabled = false;
			extendButton.IsEnabled = false;
			retractButton.IsEnabled = false;
			startCtrlButton.IsEnabled = false;
			stopCtrlButton.IsEnabled = false;

			// Make sure the morphling doesn't hit anything on its way up
			ensureMorhplingRetracted();

			// Issue the order to extend
			robotArm.extend(false);
		}
		private void retractButton_click (object sender, RoutedEventArgs e)
		{
			// Disable relevant controls during the operation
			pingButton.IsEnabled = false;
			extendButton.IsEnabled = false;
			retractButton.IsEnabled = false;
			startCtrlButton.IsEnabled = false;
			stopCtrlButton.IsEnabled = false;

			// Make sure the morphling doesn't hit anything on its way down
			ensureMorhplingRetracted();

			// Issue the order to retract
			robotArm.retract(false);
		}
		private void reloadCfgButton_click(object sender, RoutedEventArgs e)
		{
			robotArm.reloadConfig();
		}
		private void startCtrlButton_click(object sender, RoutedEventArgs e)
		{
			robotArm.enableControl(true);
		}
		private void stopCtrlButton_click(object sender, RoutedEventArgs e)
		{
			robotArm.enableControl(false);
		}
	}
}
