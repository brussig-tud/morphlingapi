﻿
//////
//
// References
//

// .NET Framework
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;



//////
//
// Implementation
//

namespace MorphlingAPI_Test
{
	/// <summary>
	/// Interaktionslogik für "App.xaml"
	/// </summary>
	public partial class App : Application
	{
		// Global variables
		public Morphling.Server morphling;
		public Morphling.RobotArm robotArm;

		// Constructor
		public App()
		{
			// Register global exception handlers
			DispatcherUnhandledException += mainThreadUnhandledExceptionHandler;
			AppDomain.CurrentDomain.UnhandledException += finalUnhandledExceptionHandler;

			// Connect to Morphling
			morphling = new Morphling.Server("192.168.0.100", 8888);
		}

		// Global events
		void onExit(object sender, ExitEventArgs e)
		{
			// Explicitly release the morphling server (we don't trust GC on this)
			if (morphling != null)
			{
				morphling.Dispose();
				morphling = null;
			}
			if (robotArm != null)
			{
				robotArm.Dispose();
				robotArm = null;
			}
		}

		// Global exception handlers
		private void mainThreadUnhandledExceptionHandler (
			object sender, DispatcherUnhandledExceptionEventArgs e
		)
		{
			MessageBox.Show(e.Exception.ToString());
			e.Handled = true;
		}

		private void finalUnhandledExceptionHandler (object sender, UnhandledExceptionEventArgs e)
		{
			MessageBox.Show(e.ExceptionObject.ToString());
		}
	}
}
